//
//  BranchesViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class BranchesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var selectedRow = 0
    var branchID = ""
    var branches = GetBranches(){
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Branches", comment: "")

        tableView.delegate = self
        tableView.dataSource = self
        
        Services.getBranches(vc: self, completionHundler: handleResponse(data:error:))
    }
    @IBAction func confirmBranchButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ConfirmCartViewController") as! ConfirmCartViewController
        vc.branchID = self.branchID
        vc.isDelivery = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func handleResponse (data : GetBranches?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            branches = data!
            print(branches)
        }
    }
}
extension BranchesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branches.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BranchesTableViewCell", for: indexPath) as! BranchesTableViewCell
        cell.branchName.text = branches.product?[indexPath.row].name
        if indexPath.row == selectedRow {
            cell.checkedBranchImg.image = UIImage(named: "checkSquare")
            self.branchID = branches.product?[selectedRow].branch_id ?? ""
        }else{
            cell.checkedBranchImg.image = UIImage(named: "square")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        tableView.reloadData()
    }
    
}
