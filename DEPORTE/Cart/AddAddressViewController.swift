//
//  AddAddressViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class AddAddressViewController: BaseViewController {
    
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var region: UITextField!
    @IBOutlet weak var block: UITextField!
    @IBOutlet weak var road: UITextField!
    @IBOutlet weak var building: UITextField!
    @IBOutlet weak var flatNumber: UITextField!
    @IBOutlet weak var notes: UITextField!
    
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var blockLabel: UILabel!
    @IBOutlet weak var roadLabel: UILabel!
    
    var countryName = ""
    var countryID = ""
    var roadFromMap = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = NSLocalizedString("Add Address", comment: "")
        self.road.text = roadFromMap
        self.countryName = UserDefaults.standard.string(forKey: "countryName") ?? ""
        self.countryID = UserDefaults.standard.string(forKey: "countryID") ?? ""
        phoneNum.text = UserDefaults.standard.string(forKey: "clientPhone") ?? ""
        country.text = UserDefaults.standard.string(forKey: "countryName") ?? ""
        NotificationCenter.default.addObserver(self, selector: #selector(self.getRegion(notification:)), name: Notification.Name("getCountry") , object: nil)
        fixTextFieldsAlignment()
        setLangForSomeWords()

    }
    
    func fixTextFieldsAlignment(){
        phoneNum.setAlignment(lang: LanguageManager.shared.currentLanguage)
        country.setAlignment(lang: LanguageManager.shared.currentLanguage)
        region.setAlignment(lang: LanguageManager.shared.currentLanguage)
        block.setAlignment(lang: LanguageManager.shared.currentLanguage)
        road.setAlignment(lang: LanguageManager.shared.currentLanguage)
        building.setAlignment(lang: LanguageManager.shared.currentLanguage)
        flatNumber.setAlignment(lang: LanguageManager.shared.currentLanguage)
        notes.setAlignment(lang: LanguageManager.shared.currentLanguage)
    }
    
    func setLangForSomeWords(){
        if self.countryID == "1"{
            regionLabel.text = NSLocalizedString("Region", comment: "")
            blockLabel.text = NSLocalizedString("Block", comment: "")
            roadLabel.text = NSLocalizedString("Road", comment: "")
        }else{
            regionLabel.text = NSLocalizedString("City", comment: "")
            blockLabel.text = NSLocalizedString("District", comment: "")
            roadLabel.text = NSLocalizedString("Street", comment: "")
        }
    }
    
    @objc func getRegion(notification: NSNotification) {
        self.countryName = notification.userInfo?["countryName"] as? String ?? ""
        self.countryID = notification.userInfo?["counntryID"] as? String ?? ""
        self.country.text = countryName
        setLangForSomeWords()
    }
    
    @IBAction func addAddressButton(_ sender: Any) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        print(countryID)
        Services.addAdress(clientPhone: phoneNum.text ?? "", lat: "", lang: "", countryID: countryID, region_id: region.text ?? "", block: block.text ?? "", road: road.text ?? "", building: building.text ?? "", flat_number: flatNumber.text ?? "", note: notes.text ?? "" , client_id: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
    func handleResponse (data : AddAddress?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            makeOkAlert(title: NSLocalizedString("You have add your adress", comment: ""), SubTitle: "", Image: UIImage())
            let vc = storyboard?.instantiateViewController(identifier: "TheAddressesViewController") as! TheAddressesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func countryButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddCountryViewController") as! AddCountryViewController
        self.present(vc, animated: true)
    }
    
}
