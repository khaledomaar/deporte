//
//  AddRegionViewController.swift
//  Anhar
//
//  Created by khaled omar on 4/30/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import UIKit

class AddCountryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var allCountries = [CountryData](){
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        Services.lookUp(vc: self, completionHundler: handleResponse(data:error:))
    }
    
    func handleResponse (data : SortFilterOptions?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herrorb
            self.allCountries = (data?.countires)!
            print(allCountries)
        }
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}
extension AddCountryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell", for: indexPath) as! CountryTableViewCell
        cell.regionName.text = allCountries[indexPath.row].name
        cell.countryImage.sd_setImage(with: URL(string: allCountries[indexPath.row].flag ?? ""))
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = ["countryName" : allCountries[indexPath.row].name , "counntryID" : allCountries[indexPath.row].id]
        NotificationCenter.default.post(name: Notification.Name("getCountry"), object: nil, userInfo: dict)
        self.dismiss(animated: true, completion: nil)
    }
    
}
