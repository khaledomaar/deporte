//
//  CartViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyCartView: UIView!
    
    var cart = GetCart(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var deleteItemFromCart = lightResponse(){
        didSet{
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getCart(clientID: ID, addressID: "", vc: self, completionHundler: handleResponse(data:error:))
            tableView.reloadData()
        }
    }
    var emptyCart = lightResponse(){
        didSet{
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getCart(clientID: ID, addressID: "", vc: self, completionHundler: handleResponse(data:error:))
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Cart", comment: "")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        emptyCartView.isHidden = true

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.cartButtonClickable = true

    }
    override func viewWillAppear(_ animated: Bool) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getCart(clientID: ID, addressID: "", vc: self, completionHundler: handleResponse(data:error:))
        emptyCartView.isHidden = true
    }
    
    @IBAction func deleteAllCartButton(_ sender: Any) {
        let alert = UIAlertController(title: "", message: NSLocalizedString("Do you want to delete all items from cart ?", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                Services.deleteAllCart(clientID: ID, vc: self, completionHundler: self.handleEmptyCartResponse(data:error:))
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }

    func handleEmptyCartResponse (data : lightResponse?, error : String?){
    if error != nil {
        // lw fe error
        self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
    }else if error == nil && data == nil {
        // lw mfesh wla data wla error
        self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
    }else {
        // lw fe data w mfes herror
        emptyCart = data!
        print(emptyCart)
        self.makeOkAlert(title: NSLocalizedString("Cart is empty", comment: ""), SubTitle: "", Image: UIImage())
    }
}
    
    func handleResponse (data : GetCart?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            cart = data!
            AllCartData.cartData = data!
            print(cart)
            var cartItemsCount = (cart.product?.count ?? 0 ) - 1
            if cartItemsCount < 0 { cartItemsCount = 0 }
            UserDefaults.standard.set("\(cartItemsCount)", forKey: "cartItems")
            self.addleftBarButtonItemWithBadge(itemvalue: "\(cartItemsCount)")
            if cart.product?.count == 0 {
                tableView.isHidden = true
                emptyCartView.isHidden = false
            }else{
                tableView.isHidden = false
                emptyCartView.isHidden = true
            }
            self.cartButtonClickable = false
        }
    }
    func handleDeleteCartResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            deleteItemFromCart = data!
            print(deleteItemFromCart)
            self.makeOkAlert(title: NSLocalizedString("Product has Deleted from cart", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
    func handleEditCartResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getCart(clientID: ID, addressID: "", vc: self, completionHundler: handleResponse(data:error:))
        }
    }
}
extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (cart.product?.count ?? 1) - 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemTableViewCell", for: indexPath) as! CartItemTableViewCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "CartCostTableViewCell", for: indexPath) as! CartCostTableViewCell
        if indexPath.section == 0 {
            cell.dishName.text = cart.product?[indexPath.row].sub_category_name
            cell.unit.text = (cart.product?[indexPath.row].size_price ?? "0") + NSLocalizedString(" BHD", comment: "")
            cell.colorName.text = cart.product?[indexPath.row].color_name
            cell.sizeName.text = cart.product?[indexPath.row].size_name
            cell.amountLabel.text = cart.product?[indexPath.row].quantity
            cell.price.text = (cart.product?[indexPath.row].price ?? "0") + NSLocalizedString(" BHD", comment: "")
            cell.dishImage.sd_setImage(with: URL(string: cart.product?[indexPath.row].sub_category_images?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
            
            cell.deleteOneItemCallBack = {
                let alert = UIAlertController(title: "", message: NSLocalizedString("Do you want to delete this item from cart ?", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                    Services.DeletedishFromCart(clientID: ID, cartID: self.cart.product?[indexPath.row].cart_id ?? "", vc: self, completionHundler: self.handleDeleteCartResponse(data:error:))
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            cell.plusORminusQuantityCallBack = { quantity in
                let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                let cartID = self.cart.product?[indexPath.row].cart_id ?? ""
                Services.editCart(clientID: ID, quantity: quantity, cartID: cartID, vc: self, completionHundler: self.handleEditCartResponse(data:error:))
            }
            return cell
        }else {
            cell2.vatPersentage.text = NSLocalizedString("VAT", comment: "") + " (\(cart.product?.last?.vat ?? "0")%) :"
            cell2.vatValue.text = "\(cart.product?.last?.vat_value ?? "")" + NSLocalizedString(" BHD", comment: "")
            cell2.totalAfter.text = "\(cart.product?.last?.summary ?? "")" + NSLocalizedString(" BHD", comment: "")
            cell2.discountValue.text = "\(cart.product?.last?.discount_value ?? "")" + NSLocalizedString(" BHD", comment: "")
            cell2.dicountPersentage.text = NSLocalizedString("Discount" , comment: "") + " (\(cart.product?.last?.discount_percentage ?? 0)%) :"
            cell2.totalAmount.text = "\(cart.product?.last?.total_amount ?? "")" + NSLocalizedString(" BHD", comment: "")
            if cart.product?.last?.check_discount == "0"{
                cell2.discountStackView.isHidden = true
            }else{
                cell2.discountStackView.isHidden = false
            }
            cell2.continueToBuyCallBack = {
                let vc = self.storyboard?.instantiateViewController(identifier: "PickRecievingMethodVC") as! PickRecievingMethodVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell2
        }
    }
}
class AllCartData {
    static var cartData = GetCart()
    static var deliveryID = String()
//    static var addtions = [AddtionsData]()
//    static var removes = [RemovesData]()
    static var sizesOf = SubCatSize()
    static var branchID = String()
}
