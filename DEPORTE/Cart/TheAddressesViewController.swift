//
//  TheAddressesViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class TheAddressesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var fromNotCart = false
    var newConstrain = false
    var allAdresses = GetAddress(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var deleteAdress = lightResponse(){
        didSet{
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getAddresses(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
        }
    }
    var selectedRow = 0
    var selectedOne = GetAddressData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Addresses", comment: "")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getAddresses(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
    
    func handleResponse (data : GetAddress?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            allAdresses = data!
            print(allAdresses)
        }
    }
    func handleDeleteAdressResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            deleteAdress = data!
            print(deleteAdress)
            makeOkAlert(title: NSLocalizedString("Deleted", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
}
extension TheAddressesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return allAdresses.product?.count ?? 0
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressesTableViewCell", for: indexPath) as! AddressesTableViewCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "ButtonsInAddressesCell", for: indexPath) as! ButtonsInAddressesCell
        if indexPath.section == 0 {
            cell.setDataToOutlets(data: allAdresses.product?[indexPath.row] ?? GetAddressData())
            if indexPath.row == selectedRow {
                cell.checkedBoxImage.image = UIImage(named: "checkSquare")
                self.selectedOne = self.allAdresses.product?[selectedRow] ?? GetAddressData()
            }else{
                cell.checkedBoxImage.image = UIImage(named: "square")
            }
            cell.deleteAdressCallBack = {
                let alert = UIAlertController(title: "", message: NSLocalizedString("You want to delete this adress ?", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                    let adressId = self.allAdresses.product?[indexPath.row].client_address_id ?? ""
                    
                    Services.deleteAddress(addressID: adressId, clientID: ID, vc: self, completionHundler: self.handleDeleteAdressResponse(data:error:))
                    
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            cell.editAdressCallBack = {
                let vc = self.storyboard?.instantiateViewController(identifier: "EditAddressViewController") as! EditAddressViewController
                vc.oldNum = self.allAdresses.product?[indexPath.row].client_phone ?? ""
                vc.oldCountry = self.allAdresses.product?[indexPath.row].country_name ?? ""
                vc.oldRoad = self.allAdresses.product?[indexPath.row].road ?? ""
                vc.oldBlock = self.allAdresses.product?[indexPath.row].block ?? ""
                vc.oldRegion = self.allAdresses.product?[indexPath.row].region_name ?? ""
                vc.oldBuilding = self.allAdresses.product?[indexPath.row].building ?? ""
                vc.oldFlatNumber = self.allAdresses.product?[indexPath.row].flat_number ?? ""
                vc.oldNotes = self.allAdresses.product?[indexPath.row].note ?? ""
                vc.addressID = self.allAdresses.product?[indexPath.row].client_address_id ?? ""
                    
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        }else{
            if allAdresses.product?.count == 0{
                cell2.confirmAddressView.isHidden = true
            }else{
                cell2.confirmAddressView.isHidden = false
            }
            cell2.addAddressCallBack = {
                let vc = self.storyboard?.instantiateViewController(identifier: "YourLocationViewController") as! YourLocationViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if self.fromNotCart == true {
                cell2.confirmAddressView.isHidden = true
                cell2.addAddressView.translatesAutoresizingMaskIntoConstraints = false
                cell2.addAddressView.widthAnchor.constraint(equalToConstant: 150).isActive = true
                cell2.addAddressView.heightAnchor.constraint(equalToConstant: 50).isActive = true
                cell2.addAddressView.centerXAnchor.constraint(equalTo: cell2.buttonsContainerView.centerXAnchor).isActive = true
                cell2.addAddressView.centerYAnchor.constraint(equalTo: cell2.buttonsContainerView.centerYAnchor).isActive = true
            }
            cell2.confirmAddressCallBack = {
                let vc = self.storyboard?.instantiateViewController(identifier: "ConfirmCartViewController") as! ConfirmCartViewController
                vc.addressData = self.selectedOne
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell2
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        tableView.reloadData()
    }
}
