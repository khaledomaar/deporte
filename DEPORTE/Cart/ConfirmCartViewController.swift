//
//  ConfirmCartViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ConfirmCartViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var cart: GetCart?{
        didSet{
            tableView.reloadData()
        }
    }
    var payment = PaymentMethods(){
        didSet{
            tableView.reloadData()
        }
    }
    var allowedPayment = [PaymentData](){
        didSet{
            tableView.reloadData()
        }
    }
    var selectedIndex = 0
    var addressData = GetAddressData()
    var isDelivery = true
    var branchID = ""
    var addressID = ""
    var successOrderCallBack: (()->())?
    var paymentId = ""
    var usePoints = "0"
    var finishUsingTheOffer = false
    var amountAfterUsingPoints = ""
//    var chargeCost: String?
//    var totalAfterCharge = Double()
    var note: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Confirm Buying", comment: "")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
//        self.cart = AllCartData.cartData
        getCart()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.finishUsingTheOffer = false
    }
    
    func getCart() {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getCart(clientID: ID, addressID: addressData.client_address_id ?? "", vc: self, completionHundler: handleCartResponse(data:error:))
    }
    
    func getPaymentMethods() {
        Services.getPaymentMethods(vc: self, completionHundler: handlePaymentResponse(data:error:))
    }
    
    func handleCartResponse (data : GetCart?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            cart = data!
            getPaymentMethods()
        }
    }
    
    func handleResponse (data : ConfirmOrder?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            UserDefaults.standard.removeObject(forKey: "cartItems")
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(identifier: "CustomeTabBar") as! CustomeTabBar
            vc.selectedIndex = 1
            self.view.window?.rootViewController = vc
            makeOkAlert(title: NSLocalizedString("Order Confirmed Successfully !", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
    func handlePaymentResponse (data : PaymentMethods?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.payment = data!
            for item in payment.product ?? []{
                if item.display == "1"{
                    self.allowedPayment.append(item)
                }
            }
            print(payment)
        }
    }
    func handleAvailableResponse (data : AvailableOrder?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            if data?.product?.first?.accept_orders == "1" {
                // check order sevices
                var cartIDs = [String]()
                for index in 0 ..< (self.cart?.product?.count ?? 0) - 1 {
                    print(index)
                    cartIDs.append(self.cart?.product?[index].cart_id ?? "")
                }
                let cartIDAsString = cartIDs.joined(separator:",")
                Services.checkCart(cartID: cartIDAsString, vc: self, completionHundler: handleCheckCartResponse(data:error:))
            }else{
                makeOkAlert(title: NSLocalizedString("Sorry, Orders not available right now", comment: ""), SubTitle: "", Image: UIImage())
            }
        }
    }
    func handleCheckCartResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            if isDelivery { // dlivery
                let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                let addressId = self.addressData.client_address_id ?? ""
                var cartIDs = [String]()
                for index in 0 ..< (self.cart?.product?.count ?? 0) - 1 {
                    print(index)
                    cartIDs.append(self.cart?.product?[index].cart_id ?? "")
                }
                let cartIDAsString = cartIDs.joined(separator:",")
                Services.checkOrder(clientID: ID, clientAddressID: addressId, cartID: cartIDAsString, orderID: "", vc: self, completionHundler: handleCheckOrderResponse(data:error:))
            }else {
                self.successOrderCallBack?()
            }
        }
    }
    func handleCheckOrderResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.successOrderCallBack?()
        }
    }
    func handleLoyaltyPointsResponse (data : loyaltyPoints?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            if (Double("\(data?.client_points_show_pop ?? "")") ?? 0) > (Double("\(data?.client_points ?? "")") ?? 0){
                self.usePoints = "0"
                self.successOrderCallBack?()
            }else{
                let alert = UIAlertController(title: "", message: data?.message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    self.usePoints = "1"
                    self.finishUsingTheOffer = true
                    self.amountAfterUsingPoints = data?.total_amount ?? ""
                    self.tableView.reloadData()
                    alert.dismiss(animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    self.usePoints = "0"
                    self.finishUsingTheOffer = true
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func configurePaymentMethod (cell2: ConfirmCartInformationCell){
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        let addressId = self.addressData.client_address_id ?? ""
        //        let branchID = self.cart.product!.first?.branch_id ?? ""
        var cartIDs = [String]()
        for index in 0 ..< (self.cart?.product?.count ?? 0) - 1 {
            print(index)
            cartIDs.append(self.cart?.product?[index].cart_id ?? "")
        }
        let cartIDAsString = cartIDs.joined(separator:",")
        cell2.cashBuyingCallBack = {
            let deliveryType =  AllCartData.deliveryID
            print(deliveryType)
            Services.confirmOrder(payment: "cash", deliverID: deliveryType, clientID: ID, addressID: addressId, note: self.note ?? "", cartIDs: cartIDAsString, branchId: self.branchID, usePoints: self.usePoints, vc: self, completionHundler: self.handleResponse(data:error:))
        }
        cell2.benefitBuyingCallBack = {
            let deliveryType =  AllCartData.deliveryID
            Services.confirmOrder(payment: "benefit", deliverID: deliveryType, clientID: ID, addressID: addressId, note: self.note ?? "", cartIDs: cartIDAsString, branchId: self.branchID, usePoints: self.usePoints, vc: self, completionHundler: self.handleResponse(data:error:))
        }
        cell2.debitBuyingCallBack = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentWebViewController") as! PaymentWebViewController
            vc.paymentType = "debit"
            vc.cartID = cartIDAsString
            vc.deliveryID = AllCartData.deliveryID
            vc.addID = addressId
            vc.branchID = self.branchID
            vc.usePoints = self.usePoints
            vc.note = self.note ?? ""
            let totalAmount = self.cart?.product?.last?.summary ?? ""
//            if self.isDelivery{
//                totalAmount = "\(self.totalAfterCharge)"
//            }
            vc.totalAmount = totalAmount
            vc.urlToOPen = "https://deporte-bh.com/debit/init.php?amount=\(totalAmount)"
            print("http://www.burgerzoom.com/debit_payment/init.php?amount=\(totalAmount)")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell2.creditBuyingCallBack = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentWebViewController") as! PaymentWebViewController
            let randomInt = Int.random(in: 0..<9999999999)
            vc.paymentType = "credit"
            vc.cartID = cartIDAsString
            vc.deliveryID = AllCartData.deliveryID
            vc.addID = addressId
            vc.branchID = self.branchID
            vc.usePoints = self.usePoints
            vc.note = self.note ?? ""
            let totalAmpunt = self.cart?.product?.last?.summary ?? ""
//            if self.isDelivery{
//                totalAmpunt = "\(self.totalAfterCharge)"
//            }
            vc.totalAmount = totalAmpunt
            vc.urlToOPen = "http://deporte-bh.com/credit/init.php?amount=\(totalAmpunt)&orderID=\(randomInt)&client_id=\(ID)"
            print("http://burgerzoom.com/credit_payment/init.php?client_id=\(ID)&orderID=\(randomInt)&amount=\(totalAmpunt)")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func ConfirmOrderAction(){
        if UserDefaults.standard.string(forKey: "loyaltySetting") == "0"{
            Services.available(vc: self, completionHundler: self.handleAvailableResponse(data:error:))
        }else{
            if self.finishUsingTheOffer == false{
                var cartIDs = [String]()
                for index in 0 ..< (self.cart?.product?.count ?? 0) - 1 {
                    print(index)
                    cartIDs.append(self.cart?.product?[index].cart_id ?? "")
                }
                let cartIDAsString = cartIDs.joined(separator:",")
                let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                Services.getLoyaltyPoints(cartID: cartIDAsString, clientID: ID, addressID: self.addressData.client_address_id ?? "", vc: self, completionHundler: self.handleLoyaltyPointsResponse(data:error:))
            }else{
                Services.available(vc: self, completionHundler: self.handleAvailableResponse(data:error:))
            }
        }
    }
}

extension ConfirmCartViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if cart != nil {
            return 3
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (cart?.product?.count ?? 0) - 1
        }else if section == 1{
            var num = 0
            for item in payment.product ?? []{
                if item.display == "1"{
                    num = num + 1
                }
            }
            return num
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemInConfirmCartCell", for: indexPath) as! ItemInConfirmCartCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "ConfirmCartInformationCell", for: indexPath) as!ConfirmCartInformationCell
        let paymentCell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodTableViewCell", for: indexPath) as!PaymentMethodTableViewCell
        
        if indexPath.section == 0 {
            cell.dishName.text = cart?.product?[indexPath.row].sub_category_name
            cell.unit.text = (cart?.product?[indexPath.row].size_price ?? "0") + NSLocalizedString(" BHD", comment: "")
            cell.quantity.text = cart?.product?[indexPath.row].quantity
            cell.colorName.text = cart?.product?[indexPath.row].color_name
            cell.sizeName.text = cart?.product?[indexPath.row].size_name
            cell.price.text = (cart?.product?[indexPath.row].price ?? "0") + NSLocalizedString(" BHD", comment: "")
            cell.dishImage.sd_setImage(with: URL(string: cart?.product?[indexPath.row].sub_category_images?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
            return cell
        }else if indexPath.section == 1{
            paymentCell.paymentName.text = allowedPayment[indexPath.row].name
            if selectedIndex == indexPath.row {
                paymentCell.checkedImage.image = UIImage(named: "dot-circle")
                self.paymentId = allowedPayment[indexPath.row].deliver_id ?? ""
            }else{
                paymentCell.checkedImage.image = UIImage(named: "circle")
            }
            if allowedPayment[indexPath.row].deliver_id == "1" {
                paymentCell.firstImage.image = UIImage(named: "money")
                paymentCell.secondImage.image = UIImage()
                paymentCell.thirdImage.image = UIImage()
            }else if allowedPayment[indexPath.row].deliver_id == "2" {
                paymentCell.firstImage.image = UIImage(named: "images1")
                paymentCell.secondImage.image = UIImage()
                paymentCell.thirdImage.image = UIImage()
            }else if allowedPayment[indexPath.row].deliver_id == "3" {
                paymentCell.firstImage.image = UIImage(named: "jcbLogoSvg")
                paymentCell.secondImage.image = UIImage(named: "visa")
                paymentCell.thirdImage.image = UIImage(named: "58482354Cef1014C0B5E49C0")
            }else if allowedPayment[indexPath.row].deliver_id == "4" {
                paymentCell.firstImage.image = UIImage(named: "images1")
            }
            return paymentCell
        }else{
            cell2.setOrderCostDetailsToOutlets(data: cart?.product?.last ?? GetCartData())
            if self.isDelivery {
                cell2.chargeStackView.isHidden = false
                cell2.addressStackView.isHidden = false
                cell2.setAddressDataToOutLets(data: addressData)
                cell2.chargeCost.text = "\(cart?.product?.last?.charge ?? "0")" + NSLocalizedString(" BHD", comment: "")
//                totalAfterCharge = (Double("\(addressData.charge ?? "")") ?? 0) + (Double("\(cart.product?.last?.summary ?? "")") ?? 0)
                cell2.totalAfter.text = "\(cart?.product?.last?.summary ?? "0")" + NSLocalizedString(" BHD", comment: "")// String(format: "%.3f", totalAfterCharge) + NSLocalizedString(" BHD", comment: "")
            }else{
                cell2.chargeStackView.isHidden = true
                cell2.addressStackView.isHidden = true
                cell2.addressStackView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            }
            if usePoints == "1"{
                cell2.totalAfter.text = "\(amountAfterUsingPoints)" + NSLocalizedString(" BHD", comment: "")
            }
            cell2.confirmBuyCallBack = { paymentID, note in
                self.ConfirmOrderAction()
                self.note = note
                self.successOrderCallBack = {
                    if self.paymentId == "1"{
                        cell2.cashBuyingCallBack?()
                    }else if self.paymentId == "3"{
                        cell2.creditBuyingCallBack?()
                    }else if self.paymentId == "2"{
                        cell2.debitBuyingCallBack?()
                    }else if self.paymentId == "4"{
                        cell2.benefitBuyingCallBack?()
                    }
                }
            }
            configurePaymentMethod(cell2: cell2)
            return cell2
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            selectedIndex = indexPath.row
            self.tableView.reloadData()
        }
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
