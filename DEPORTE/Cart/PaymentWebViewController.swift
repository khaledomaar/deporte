//
//  PaymentWebViewController.swift
//  ZoomBurger
//
//  Created by khaled omar on 8/26/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class PaymentWebViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    var urlToOPen = ""
    var paymentType = ""
    var sessionID = ""
    var debitPaymentId = "" // payment id for service (add payment) -> debit confirmation service
    var orderID = ""
    var totalAmount = ""
    // confirm order Vars
    var deliveryID = ""
    var cartID = ""
    var addID = ""
    var branchID = ""
    var usePoints = "0"
    var note = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: URL(string: self.urlToOPen)!)
        self.webView.loadRequest(request)
        webView.delegate = self
        print(deliveryID)
        print(addID)
        print(branchID)
    }
    
    //MARK:- DEBIT Services
    func doDebitPayment(){
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.confirmOrder(payment: "debit", deliverID: deliveryID, clientID: ID, addressID: addID, note: note, cartIDs: cartID, branchId: branchID, usePoints: usePoints, vc: self, completionHundler: self.handleConfirmDebitResponse(data:error:))
    }

    func handleConfirmDebitResponse (data : ConfirmOrder?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.addPayment(client_id: ID, order_id: "\(data?.product?.first?.order_id ?? 0)", value: "\(totalAmount)", result: "success", payment_id: self.debitPaymentId, branch_id: branchID, vc: self, completionHundler: handleAddPaymentResponse(data: error:))
        }
    }

    func handleAddPaymentResponse (data : ConfirmOrder?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "CustomeTabBar") as! CustomeTabBar
            vc.selectedIndex = 1
            self.view.window?.rootViewController = vc
            self.makeOkAlert(title: NSLocalizedString("Order Confirmed Successfully !", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
    //MARK:- Credit Services
    func doCreditPayment(){
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.paymentResult(client_id: ID, session_id: self.sessionID, vc: self, completionHundler: handleDoCreditResponse(data:error:))
    }
    func handleDoCreditResponse (data : CreditPaymentResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.confirmCreditPayment()
        }
    }
    
    func confirmCreditPayment(){
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.confirmOrder(payment: "credit", deliverID: deliveryID, clientID: ID, addressID: addID, note: note, cartIDs: cartID, branchId: branchID, usePoints: usePoints, vc: self, completionHundler: self.handleConfirmCreditResponse(data:error:))
    }
    func handleConfirmCreditResponse (data : ConfirmOrder?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "CustomeTabBar") as! CustomeTabBar
            vc.selectedIndex = 1
            self.view.window?.rootViewController = vc
            self.makeOkAlert(title: NSLocalizedString("Order Confirmed Successfully !", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
}

extension PaymentWebViewController: UIWebViewDelegate{
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        if (request.url?.absoluteString.contains("error"))! || (request.url?.absoluteString.contains("failed"))!{
            print("Failed")
            return true
        }else if (request.url?.absoluteString.contains("Success"))!{
            if self.paymentType == "debit" {
                self.doDebitPayment()
                print("successssseeeddddddd")
            }
            return true
        }else if (request.url?.absoluteString.contains("SESSION"))!{
            let sessionURL = request.url?.absoluteString
            let index = sessionURL!.index(sessionURL!.endIndex, offsetBy: -31)
            let mySubstring = sessionURL![index...]
            self.sessionID = String(mySubstring)
            print(sessionID)
            return true
        }else if (request.url?.absoluteString.contains("sessionVersion"))!{
            self.doCreditPayment()
            return true
        }else if (request.url?.absoluteString.contains("PaymentID"))!{
            let paymentURL = request.url?.absoluteString
            if let textRange = paymentURL!.range(of: "PaymentID=") {
                let mySubstring = paymentURL![textRange.upperBound...]
                self.debitPaymentId = String(mySubstring)
                print(debitPaymentId)
            }
            return true
        }else{
            return true
        }
    }
}
