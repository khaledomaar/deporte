//
//  ViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class PickRecievingMethodVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var deliverWayes = DeliverWays() {
        didSet{
            tableView.reloadData()
        }
    }
    var allowedDeliveryWayes = [DeliverWaysData]()
    
    var allAdresses = GetAddress()
    var selectedRow = 0
    
    var images = ["-e-Hue_Saturation 1496" , "-e-Hue_Saturation 1496" , "-e-Hue_Saturation 1496"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        Services.getDeliverWayes(vc: self, completionHundler: handleResponse(data:error:))
    }
    
    func handleResponse (data : DeliverWays?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            deliverWayes = data!
            for item in deliverWayes.product ?? []{
                if item.display == "1"{
                    self.allowedDeliveryWayes.append(item)
                }
            }
            print(deliverWayes)
        }
    }
    func handleAddressResponse (data : GetAddress?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            allAdresses = data!
            print(allAdresses)
            if allAdresses.product?.count == 0 {
                let vc = self.storyboard?.instantiateViewController(identifier: "YourLocationViewController") as! YourLocationViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = self.storyboard?.instantiateViewController(identifier: "TheAddressesViewController") as! TheAddressesViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension PickRecievingMethodVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var num = 0
        for item in deliverWayes.product ?? []{
            if item.display == "1"{
                num = num + 1
            }
        }
        return num
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliverWaysTableViewCell") as! DeliverWaysTableViewCell
        cell.deliverWayName.text = allowedDeliveryWayes[indexPath.row].name
        if allowedDeliveryWayes[indexPath.row].deliver_id == "1"{
            cell.deliverImage.image = UIImage(named: images[0])
        }else if allowedDeliveryWayes[indexPath.row].deliver_id == "2"{
            cell.deliverImage.image = UIImage(named: images[1])
        }else if allowedDeliveryWayes[indexPath.row].deliver_id == "3"{
            cell.deliverImage.image = UIImage(named: images[2])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AllCartData.deliveryID = allowedDeliveryWayes[indexPath.row].deliver_id ?? ""
        if allowedDeliveryWayes[indexPath.row].deliver_id == "1"{
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getAddresses(clientID: ID, vc: self, completionHundler: handleAddressResponse(data:error:))
        }else if allowedDeliveryWayes[indexPath.row].deliver_id == "2"{
            let vc = self.storyboard?.instantiateViewController(identifier: "ConfirmCartViewController") as! ConfirmCartViewController
            vc.isDelivery = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if allowedDeliveryWayes[indexPath.row].deliver_id == "3"{
            let vc = self.storyboard?.instantiateViewController(identifier: "ConfirmCartViewController") as! ConfirmCartViewController
            vc.isDelivery = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
