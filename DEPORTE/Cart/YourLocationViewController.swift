//
//  YourLocationViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class YourLocationViewController: BaseViewController {
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
    private let locationManager = CLLocationManager()
    
    var selectedCoordinate : CLLocationCoordinate2D?
//    lazy var camera = GMSCameraPosition.camera(withLatitude: 26.06861, longitude: 50.50389, zoom: 10.0)
//    lazy var gmapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
      // 1
      let geocoder = GMSGeocoder()
        
      // 2
      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        guard let address = response?.firstResult(), let lines = address.lines else {
          return
        }
          
        // 3
        self.addressLabel.text = lines.joined(separator: "\n")
          
        // 4
        UIView.animate(withDuration: 0.25) {
          self.view.layoutIfNeeded()
        }
      }
    }

    @IBAction func mapNextButtton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "AddAddressViewController") as! AddAddressViewController
        vc.roadFromMap = self.addressLabel.text ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
// MARK: - CLLocationManagerDelegate
//1
extension YourLocationViewController: CLLocationManagerDelegate {
  // 2
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // 3
    guard status == .authorizedWhenInUse else {
      return
    }
    // 4
    locationManager.startUpdatingLocation()
      
    //5
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
  }
  
  // 6
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
      
    // 7
    mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      
    // 8
    locationManager.stopUpdatingLocation()
  }
}
// MARK: - GMSMapViewDelegate
extension YourLocationViewController: GMSMapViewDelegate {
    
  func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    reverseGeocodeCoordinate(position.target)
  }

}

