//
//  DeliverWaysTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/11/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class DeliverWaysTableViewCell: UITableViewCell {

    @IBOutlet weak var deliverImage: UIImageView!
    @IBOutlet weak var deliverWayName: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
