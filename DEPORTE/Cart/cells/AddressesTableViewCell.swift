//
//  AddressesTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class AddressesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkedBoxImage: UIImageView!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var road: UILabel!
    @IBOutlet weak var block: UILabel!
    @IBOutlet weak var home: UILabel!
    @IBOutlet weak var flatNumber: UILabel!
    
    var deleteAdressCallBack: (()->())?
     var editAdressCallBack: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setDataToOutlets(data: GetAddressData){
        if data.country_id == "1"{
            country.text = NSLocalizedString("Country: ", comment: "") + "\(data.country_name ?? "")"
            region.text = NSLocalizedString("Region: ", comment: "") + "\(data.region_name ?? "")"
            road.text = NSLocalizedString("Road: ", comment: "") + "\(data.road ?? "")"
            block.text = NSLocalizedString("Block: ", comment: "") + "\(data.block ?? "")"
            home.text = NSLocalizedString("Building: ", comment: "") + "\(data.building ?? "")"
            flatNumber.text = NSLocalizedString("Flat Number: ", comment: "") + "\(data.flat_number ?? "")"
        }else{
            country.text = NSLocalizedString("Country: ", comment: "") + "\(data.country_name ?? "")"
            region.text = NSLocalizedString("City: ", comment: "") + "\(data.region_name ?? "")"
            road.text = NSLocalizedString("Street: ", comment: "") + "\(data.road ?? "")"
            block.text = NSLocalizedString("District: ", comment: "") + "\(data.block ?? "")"
            home.text = NSLocalizedString("Building: ", comment: "") + "\(data.building ?? "")"
            flatNumber.text = NSLocalizedString("Flat Number: ", comment: "") + "\(data.flat_number ?? "")"
        }
    }
    
    @IBAction func deleteAdressButton(_ sender: Any) {
        self.deleteAdressCallBack?()
    }
    @IBAction func editAdressButton(_ sender: Any) {
        self.editAdressCallBack?()
    }
}
