//
//  CartCostTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class CartCostTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var dicountPersentage: UILabel!
    @IBOutlet weak var discountValue: UILabel!
    @IBOutlet weak var vatPersentage: UILabel!
    @IBOutlet weak var vatValue: UILabel!
    @IBOutlet weak var totalAfter: UILabel!
    @IBOutlet weak var discountStackView: UIStackView!
    
    var continueToBuyCallBack: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func continueToBuy(_ sender: Any) {
        self.continueToBuyCallBack?()
    }
    
}
