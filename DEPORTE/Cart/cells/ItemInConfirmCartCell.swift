//
//  ItemInConfirmCartCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ItemInConfirmCartCell: UITableViewCell {
    
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var colorName: UILabel!
    @IBOutlet weak var sizeName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
