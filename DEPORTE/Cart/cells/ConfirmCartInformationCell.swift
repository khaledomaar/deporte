//
//  ConfirmCartInformationCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ConfirmCartInformationCell: UITableViewCell {
    
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var dicountPersentage: UILabel!
    @IBOutlet weak var discountValue: UILabel!
    @IBOutlet weak var vatPersentage: UILabel!
    @IBOutlet weak var vatValue: UILabel!
    @IBOutlet weak var chargeCost: UILabel!
    @IBOutlet weak var totalAfter: UILabel!
    @IBOutlet weak var chargeStackView: UIStackView!
    @IBOutlet weak var discountStackView: UIStackView!
    
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var block: UILabel!
    @IBOutlet weak var road: UILabel!
    @IBOutlet weak var appartment: UILabel!
    @IBOutlet weak var flatNumber: UILabel!
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var note: UITextField!
    
    var paymentId = "1"
    var confirmBuyCallBack: ((_ paymentID: String, _ note: String)->())?
    var cashBuyingCallBack: (()->())?
    var debitBuyingCallBack: (()->())?
    var creditBuyingCallBack: (()->())?
    var benefitBuyingCallBack: (()->())?
    
    func setAddressDataToOutLets(data: GetAddressData){
        if data.country_id == "1"{
            country.text = NSLocalizedString("Country: ", comment: "") + "  \(data.country_name ?? "")"
            region.text = NSLocalizedString("Region: ", comment: "") + "  \(data.region_name ?? "")"
            block.text = NSLocalizedString("Block: ", comment: "") + "  \(data.block ?? "")"
            road.text = NSLocalizedString("Road: ", comment: "") + "  \(data.road ?? "")"
            appartment.text = NSLocalizedString("Building: ", comment: "") + "  \(data.building ?? "")"
            flatNumber.text = NSLocalizedString("Flat Number: ", comment: "") + "  \(data.flat_number ?? "")"
            addressStackView.isHidden = false
            addressStackView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        }else{
            country.text = NSLocalizedString("Country: ", comment: "") + "  \(data.country_name ?? "")"
            region.text = NSLocalizedString("City: ", comment: "") + "  \(data.region_name ?? "")"
            block.text = NSLocalizedString("District: ", comment: "") + "  \(data.block ?? "")"
            road.text = NSLocalizedString("Street: ", comment: "") + "  \(data.road ?? "")"
            appartment.text = NSLocalizedString("Building: ", comment: "") + "  \(data.building ?? "")"
            flatNumber.text = NSLocalizedString("Flat Number: ", comment: "") + "  \(data.flat_number ?? "")"
            addressStackView.isHidden = false
            addressStackView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        }
    }
    
    func setOrderCostDetailsToOutlets(data: GetCartData){
        vatPersentage.text = NSLocalizedString("VAT", comment: "") + " (\(data.vat ?? "0")%) :"
        vatValue.text = "\(data.vat_value ?? "")" + NSLocalizedString(" BHD", comment: "")
        totalAfter.text = "\(data.summary ?? "")" + NSLocalizedString(" BHD", comment: "")
        discountValue.text = "\(data.discount_value ?? "")" + NSLocalizedString(" BHD", comment: "")
        dicountPersentage.text = NSLocalizedString("Discount" , comment: "") + " (\(data.discount_percentage ?? 0)%) :"
        totalAmount.text = "\(data.total_amount ?? "")" + NSLocalizedString(" BHD", comment: "")
        if data.check_discount == "0"{
            discountStackView.isHidden = true
        }else{
            discountStackView.isHidden = false
        }
    }

    @IBAction func checkOutButton(_ sender: Any) {
        self.confirmBuyCallBack?(paymentId, note.text ?? "")
    }
    
}
