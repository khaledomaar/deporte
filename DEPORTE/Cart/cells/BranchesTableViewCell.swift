//
//  BranchesTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class BranchesTableViewCell: UITableViewCell {

    @IBOutlet weak var branchName: UILabel!
    @IBOutlet weak var checkedBranchImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
