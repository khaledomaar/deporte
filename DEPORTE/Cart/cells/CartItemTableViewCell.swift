//
//  CartItemTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/7/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class CartItemTableViewCell: UITableViewCell {

    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var colorName: UILabel!
    @IBOutlet weak var sizeName: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var deleteOneItemCallBack: (()->())?
    var plusORminusQuantityCallBack : ((_: String)->())?

    var quantity : String = "1" {
        didSet{
            amountLabel.text = quantity
            self.plusORminusQuantityCallBack?(quantity)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteOneItemButton(_ sender: Any) {
        self.deleteOneItemCallBack?()
    }
    @IBAction func plusButton(_ sender: Any) {
        quantity = "\(Int(amountLabel.text!)! + 1)"
    }
    @IBAction func minusButton(_ sender: Any) {
        if amountLabel.text != "1" {
            quantity = "\(Int(amountLabel.text!)! - 1)"
        }else{
            print("eror")
        }
    }
    
    
}
