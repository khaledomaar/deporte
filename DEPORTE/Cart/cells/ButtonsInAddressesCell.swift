//
//  ButtonsInAddressesCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ButtonsInAddressesCell: UITableViewCell {
    
    @IBOutlet weak var buttonsContainerView: UIView!
    @IBOutlet weak var confirmAddressView: UIView!
    @IBOutlet weak var addAddressView: UIView!
    
    var addAddressCallBack: (()->())?
    var confirmAddressCallBack: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func addAddressButton(_ sender: Any) {
        self.addAddressCallBack?()
    }
    @IBAction func confirmAddressButton(_ sender: Any) {
        self.confirmAddressCallBack?()
    }
}
