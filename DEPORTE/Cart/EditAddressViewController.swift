//
//  EditAddressViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class EditAddressViewController: BaseViewController {
    
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var region: UITextField!
    @IBOutlet weak var block: UITextField!
    @IBOutlet weak var road: UITextField!
    @IBOutlet weak var building: UITextField!
    @IBOutlet weak var flatNumber: UITextField!
    @IBOutlet weak var notes: UITextField!
    
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var blockLabel: UILabel!
    @IBOutlet weak var roadLabel: UILabel!
    
    var oldNum = ""
    var oldCountry = ""
    var oldRegion = ""
    var oldBlock = ""
    var oldRoad = ""
    var oldBuilding = ""
    var oldFlatNumber = ""
    var oldNotes = ""
    var addressID = ""
    var countryName = ""
    var countryID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Edit Address", comment: "")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getRegion(notification:)), name: Notification.Name("getCountry") , object: nil)
        fixTextFieldsAlignment()
        setLangForSomeWords()
    }
    func fixTextFieldsAlignment(){
        phoneNum.setAlignment(lang: LanguageManager.shared.currentLanguage)
        country.setAlignment(lang: LanguageManager.shared.currentLanguage)
        region.setAlignment(lang: LanguageManager.shared.currentLanguage)
        block.setAlignment(lang: LanguageManager.shared.currentLanguage)
        road.setAlignment(lang: LanguageManager.shared.currentLanguage)
        building.setAlignment(lang: LanguageManager.shared.currentLanguage)
        flatNumber.setAlignment(lang: LanguageManager.shared.currentLanguage)
        notes.setAlignment(lang: LanguageManager.shared.currentLanguage)
    }
    
    func setLangForSomeWords(){
        if self.countryID == "1"{
            regionLabel.text = NSLocalizedString("Region", comment: "")
            blockLabel.text = NSLocalizedString("Block", comment: "")
            roadLabel.text = NSLocalizedString("Road", comment: "")
        }else{
            regionLabel.text = NSLocalizedString("City", comment: "")
            blockLabel.text = NSLocalizedString("District", comment: "")
            roadLabel.text = NSLocalizedString("Street", comment: "")
        }
    }
    
    @objc func getRegion(notification: NSNotification) {
        self.countryName = notification.userInfo?["countryName"] as? String ?? ""
        self.countryID = notification.userInfo?["counntryID"] as? String ?? ""
        self.country.text = countryName
        setLangForSomeWords()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        phoneNum.text = oldNum
//        country.text = oldCountry
        region.text = oldRegion
        block.text = oldBlock
        road.text = oldRoad
        building.text = oldBuilding
        flatNumber.text = oldFlatNumber
        notes.text = oldNotes
        
    }
    
    @IBAction func countryButton(_ sender: Any){
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddCountryViewController") as! AddCountryViewController
        self.present(vc, animated: true)

    }
    
    
    @IBAction func editAdressButton(_ sender: Any) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.editAddress(addressID: addressID, clientPhone: phoneNum.text ?? "", lat: "", lang: "", countryID: countryID, regionName: region.text ?? "" , block: block.text ?? "", road: road.text ?? "", building: building.text ?? "", flat_number: flatNumber.text ?? "", note: notes.text ?? "", client_id: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
        
    func handleResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            makeOkAlert(title: NSLocalizedString("You have edit your adress", comment: ""), SubTitle: "", Image: UIImage())
            self.navigationController?.popViewController(animated: true)
        }
    }
}
