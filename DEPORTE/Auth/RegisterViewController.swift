//
//  RegisterViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/6/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class RegisterViewController: AuthBaseViewController {

    @IBOutlet weak var clientPhone: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var countryImage: UIImageView!
    
    var allCountries = [CountryData]()
    var selectedCounntryID = ""
    var countryPicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Register", comment: "")
        clientPhone.setAlignment(lang: LanguageManager.shared.currentLanguage)
        country.setAlignment(lang: LanguageManager.shared.currentLanguage)
        
        Services.lookUp(vc: self, completionHundler: handleResponse(data:error:))
        
        countryPicker = UIPickerView()
        countryPicker.dataSource = self
        countryPicker.delegate = self
        
        country.inputView = countryPicker

    }
    
    func handleResponse (data : SortFilterOptions?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            allCountries = (data?.countires)!
            print(allCountries)
        }
    }
    
    @IBAction func continueRegisterButton(_ sender: Any) {
        if clientPhone.text != "" && country.text != ""{
            let vc = self.storyboard?.instantiateViewController(identifier: "ConfirmRegisterViewController") as! ConfirmRegisterViewController
            vc.clientPhone = self.clientPhone.text ?? ""
            vc.countryID = self.selectedCounntryID
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            makeOkAlert(title: NSLocalizedString("All fields are required", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
    @IBAction func alreadyHaveAccButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension RegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        allCountries.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allCountries[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        country.text = allCountries[row].name
        self.selectedCounntryID = allCountries[row].id ?? ""
        countryImage.sd_setImage(with: URL(string: allCountries[row].flag ?? ""))
    }
}
