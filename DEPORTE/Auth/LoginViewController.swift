//
//  LoginViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/6/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class LoginViewController: AuthBaseViewController {
    
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Login", comment: "")
        
        phoneNum.setAlignment(lang: LanguageManager.shared.currentLanguage)
        password.setAlignment(lang: LanguageManager.shared.currentLanguage)
        
    }
    
    @IBAction func loginButton(_ sender: Any) {
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken") ?? ""
        if phoneNum.text != "" && password.text != "" {
            Services.userLogin(clientPassword: password.text ?? "", clientPhone: phoneNum.text ?? "" , deviceToken: fcmToken, type: "ios", phoneNumber: "", vc: self, completionHundler: handleResponse(data:error:))
        }else{
            makeOkAlert(title: NSLocalizedString("Please enter your phone number and password", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
    
    
    func handleResponse (data : Login?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            UserDefaults.standard.set(data?.product?.first?.client_id, forKey: "clientID")
            UserDefaults.standard.set(data?.product?.first?.client_name, forKey: "clientName")
            UserDefaults.standard.set(data?.product?.first?.country_data?.name, forKey: "countryName")
            UserDefaults.standard.set(data?.product?.first?.country_data?.id, forKey: "countryID")
            UserDefaults.standard.set(data?.product?.first?.client_email, forKey: "clientEmail")
            UserDefaults.standard.set(data?.product?.first?.client_phone, forKey: "clientPhone")
            UserDefaults.standard.set(data?.product?.first?.client_password, forKey: "clientPassword")
            if L102Language.currentAppleLanguage() == "en"{
                L102Language.setAppleLAnguageTo(lang: "en")
                LanguageManager.shared.setLanguage(language: .en)
                APIs.Instance.setLangTO(lang: "en")
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                self.restartApplication()
            }else{
                if data?.product?.first?.country_data?.id == "1"{
                    L102Language.setAppleLAnguageTo(lang: "ar")
                    LanguageManager.shared.setLanguage(language: .ar)
                    APIs.Instance.setLangTO(lang: "ar")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    self.restartApplication()
                }else{
                    L102Language.setAppleLAnguageTo(lang: "ar-EG")
                    LanguageManager.shared.setLanguage(language: .arEG)
                    APIs.Instance.setLangTO(lang: "ar")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    self.restartApplication()
                }
            }
            
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getCart(clientID: ID, addressID: "", vc: self, completionHundler: handleResponse(data:error:))
        }
    }
    func handleResponse (data : GetCart?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            var cartItemsCount = (data?.product?.count ?? 0 ) - 1
            if cartItemsCount < 0 { cartItemsCount = 0 }
            UserDefaults.standard.set("\(cartItemsCount)", forKey: "cartItems")
        }
    }
    func restartApplication() {
        print(L102Language.currentAppleLanguage())
        if L102Language.currentAppleLanguage() == "en"{
            Bundle.setLanguage( "en")
        }else{
            if UserDefaults.standard.string(forKey: "countryID") == "1"{
                Bundle.setLanguage( "ar")
            } else {
                Bundle.setLanguage( "ar-EG")
            }
        }
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }
    
    
    @IBAction func registerButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func forgetPasswordButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
