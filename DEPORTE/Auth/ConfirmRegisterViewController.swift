//
//  ConfirmRegisterViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/6/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ConfirmRegisterViewController: AuthBaseViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var passsword: UITextField!
    

    var clientPhone = ""
    var countryID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Register", comment: "")
        
        userName.setAlignment(lang: LanguageManager.shared.currentLanguage)
        passsword.setAlignment(lang: LanguageManager.shared.currentLanguage)

    }
    @IBAction func registerButton(_ sender: Any) {
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken") ?? ""
        if userName.text != "" && email.text != "" && passsword.text != "" {
            Services.register(clientName: userName.text ?? "", clientPassword: passsword.text ?? "", clientPhone: clientPhone, deviceToken: fcmToken, type: "ios", countryID: countryID, vc: self, completionHundler: handleResponse(data:error:))
        }else{
            makeOkAlert(title: NSLocalizedString("All fields are required", comment: "") , SubTitle: "", Image: UIImage())
        }
    }
    func handleResponse (data : Register?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            UserDefaults.standard.set(data?.product?.first?.client_id, forKey: "clientID")
            UserDefaults.standard.set(data?.product?.first?.client_name, forKey: "clientName")
            UserDefaults.standard.set(data?.product?.first?.country_data?.name, forKey: "countryName")
            UserDefaults.standard.set(data?.product?.first?.country_data?.id, forKey: "countryID")
            UserDefaults.standard.set(data?.product?.first?.client_email, forKey: "clientEmail")
            UserDefaults.standard.set(data?.product?.first?.client_phone, forKey: "clientPhone")
            UserDefaults.standard.set(data?.product?.first?.client_password, forKey: "clientPassword")
            
            let secStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = secStoryBoard.instantiateViewController(withIdentifier: "CustomeTabBar") as! CustomeTabBar
            self.view.window?.rootViewController = initialViewController
            initialViewController.selectedIndex = 0
        }
    }
}
