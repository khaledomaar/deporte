//
//  ProfileViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var phoneNum: UILabel!
    @IBOutlet weak var password: UILabel!
    
    var editDone = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Profile", comment: "")

        
    }
    override func viewWillAppear(_ animated: Bool) {
        phoneNum.text = UserDefaults.standard.string(forKey: "clientPhone")
        userName.text = UserDefaults.standard.string(forKey: "clientName")
        password.text = UserDefaults.standard.string(forKey: "clientPassword")
    }
    
    @IBAction func editInformationButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
