//
//  EditProfileViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Edit Profile", comment: "")
        
        userName.setAlignment(lang: LanguageManager.shared.currentLanguage)
        phone.setAlignment(lang: LanguageManager.shared.currentLanguage)
        password.setAlignment(lang: LanguageManager.shared.currentLanguage)


        userName.text = UserDefaults.standard.string(forKey: "clientName") ?? ""
        phone.text = UserDefaults.standard.string(forKey: "clientPhone") ?? ""
        password.text = UserDefaults.standard.string(forKey: "clientPassword") ?? ""

    }
    
    @IBAction func editProfile(_ sender: Any) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.editProfile(clientId: ID, clientName: userName.text ?? "", clientPassword: password.text ?? "", clientPhone: phone.text ?? "", vc: self, completionHundler: handleResponse(data:error:))
    }
    func handleResponse (data : EditProfile?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            makeOkAlert(title: NSLocalizedString("You have edit your profile successfully", comment: ""), SubTitle: "", Image: UIImage())
            UserDefaults.standard.set(data?.product?.first?.client_name, forKey: "clientName")
            UserDefaults.standard.set(data?.product?.first?.client_email, forKey: "clientEmail")
            UserDefaults.standard.set(data?.product?.first?.client_phone, forKey: "clientPhone")
            UserDefaults.standard.set(data?.product?.first?.client_password, forKey: "clientPassword")
            self.navigationController?.popViewController(animated: true)
        }
    }
}
