//
//  VerficationCodeViewController.swift
//  IceRocks
//
//  Created by khaled omar on 5/21/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import CBPinEntryView
import FirebaseAuth

class VerficationCodeViewController: AuthBaseViewController {

    @IBOutlet weak var verificationCodeView: CBPinEntryView!
    @IBOutlet weak var sendingLabel: UILabel!
    
    var phoneNum = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }

    @IBAction func confirmAndGoToResetVC(_ sender: Any) {
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") ?? ""
        let credential = PhoneAuthProvider.provider().credential(
        withVerificationID: verificationID,
        verificationCode: verificationCodeView.getPinAsString())
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
          if  error != nil  {
            self.makeOkAlert(title: NSLocalizedString("error", comment: ""), SubTitle: NSLocalizedString("something went wrong", comment: ""), Image: UIImage())
            print("error: \(String(describing: error?.localizedDescription))")
          }else{
            let vc = self.storyboard?.instantiateViewController(identifier: "ConfirmResetPasswordVC") as! ConfirmResetPasswordVC
            vc.phoneNum = self.phoneNum
            self.navigationController?.pushViewController(vc, animated: true)
            
            }
        }
    }
}
