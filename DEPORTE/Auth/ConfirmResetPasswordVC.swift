//
//  ConfirmResetPasswordVC.swift
//  IceRocks
//
//  Created by khaled omar on 5/21/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ConfirmResetPasswordVC: AuthBaseViewController {

    @IBOutlet weak var newPaasword: UITextField!
    @IBOutlet weak var confirmNewPass: UITextField!
    
    var phoneNum = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Reset Password", comment: "")

        // Do any additional setup after loading the view.
        
        newPaasword.setAlignment(lang: LanguageManager.shared.currentLanguage)
        confirmNewPass.setAlignment(lang: LanguageManager.shared.currentLanguage)
    }
    
    func handleResponse (data : lightResponse?, error : String?){
    if error != nil {
        // lw fe error
        self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
    }else if error == nil && data == nil {
        // lw mfesh wla data wla error
        self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
    }else {
        // lw fe data w mfes herror
        makeOkAlert(title: NSLocalizedString("You have reset your Password", comment: ""), SubTitle: "", Image: UIImage())
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func confirmResetingButton(_ sender: Any) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        if newPaasword.text != "" && confirmNewPass.text != "" && newPaasword.text == confirmNewPass.text {
            Services.recoverPassworD(phoneNum: phoneNum, clientPassword: newPaasword.text ?? "", clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
        }else{
            makeOkAlert(title: NSLocalizedString("there is an error please check again", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
}
