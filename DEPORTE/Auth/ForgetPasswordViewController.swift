//
//  ForgetPasswordViewController.swift
//  IceRocks
//
//  Created by khaled omar on 5/21/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import FirebaseAuth
import NVActivityIndicatorView
import Foundation

class ForgetPasswordViewController: AuthBaseViewController {
    
    @IBOutlet weak var phoneNuumber: UITextField!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryCode: UITextField!
    
    var numberExist = NumberExist()
    var countryPicker: UIPickerView!
    var allCountries = [CountryData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.title = NSLocalizedString("Forget Password", comment: "")
        
        phoneNuumber.setAlignment(lang: LanguageManager.shared.currentLanguage)
        countryCode.setAlignment(lang: LanguageManager.shared.currentLanguage)
        countryCode.text = "+973"
        // Do any additional setup after loading the view.
        Services.lookUp(vc: self, completionHundler: handleCountriesResponse(data:error:))
        
        countryPicker = UIPickerView()
        countryPicker.dataSource = self
        countryPicker.delegate = self
        countryCode.inputView = countryPicker
    }
    
    func handleCountriesResponse (data : SortFilterOptions?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            allCountries = (data?.countires)!
            print(allCountries)
        }
    }
    
    func handleResponse (data : NumberExist?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            numberExist = data!
            print(numberExist)
            if numberExist.exist == 1 {
                let frame = CGRect(x: self.view.frame.width / 2 , y: self.view.frame.height / 2, width: 0, height: 0)
                let activityIndicatorView = NVActivityIndicatorView(frame: frame, type: .ballScale)
                activityIndicatorView.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                activityIndicatorView.padding = 100
                self.view.addSubview(activityIndicatorView)
                activityIndicatorView.startAnimating()
                
                PhoneAuthProvider.provider().verifyPhoneNumber((countryCode.text ?? "") + (phoneNuumber.text ?? ""), uiDelegate: nil) { (verificationID, error) in
                    if let error = error {
                        activityIndicatorView.stopAnimating()
                        self.makeOkAlert(title: NSLocalizedString("This is invalid phone number", comment: ""), SubTitle: "", Image: UIImage())
                        print(error.localizedDescription)
                        return
                    }
                    // Sign in using the verificationID and the code sent to the user
                    // ...
                    activityIndicatorView.stopAnimating()
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                    let vc = self.storyboard?.instantiateViewController(identifier: "VerficationCodeViewController") as! VerficationCodeViewController
                    vc.phoneNum = self.phoneNuumber.text ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else if numberExist.exist == 0 {
                makeOkAlert(title: NSLocalizedString("This number is not registered!", comment: ""), SubTitle: "", Image: UIImage())
            }
        }
    }
    
    @IBAction func goToVerificationVCButton(_ sender: Any) {
        if countryCode.text == ""{
        }else{
            Services.checkNumberExist(clientNum: phoneNuumber.text ?? "", vc: self, completionHundler: handleResponse(data:error:))
        }
    }
}
extension ForgetPasswordViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        allCountries.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allCountries[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryCode.text = allCountries[row].code
        if let url:URL = URL(string: allCountries[row].flag ?? ""){
            countryImage.sd_setImage(with: url)
        }
    }
}
