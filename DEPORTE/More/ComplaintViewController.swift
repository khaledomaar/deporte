//
//  ComplaintViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import ImagePicker
import Alamofire
import NVActivityIndicatorView

class ComplaintViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageTitle: UITextField!
    @IBOutlet weak var messageContent: UITextField!
    
    var photosArray = [UIImage](){
        didSet{
            collectionView.reloadData()
        }
    }
    var photoKeys = [String]()
    var photoData = [Data]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Suggestions and Complains", comment: "")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func prepareImages (photosArray: [UIImage]){
        var index = 1
        for image in photosArray {
            let jpegData = image.jpegData(compressionQuality: 0.25)!
            self.photoData.append(jpegData)
            let key = "image_" + "\(index)"
            self.photoKeys.append(key)
            index += 1
        }
    }
    
    @IBAction func sendBtn () {
        if messageTitle.text != "" && messageContent.text != "" {
        let url = APIs.Instance.addCompliant()
        let ID = UserDefaults.standard.string(forKey: "clientID")!
        let params = ["title": self.messageTitle.text ?? "" , "content": messageContent.text ?? "" ,"client_id":ID]
        prepareImages(photosArray: self.photosArray)
        upload(imagesKeyArray: photoKeys, imagesDataArray: photoData, to: url, params: params)
        }else{
            makeOkAlert(title: NSLocalizedString("Please write your message title and message", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
    
    @IBAction func pickImageButton(_ sender: Any) {
        let imagePickerController = ImagePickerController()
        imagePickerController.imageLimit = 5
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
}
extension ComplaintViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ImagePickerDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImageCollectionViewCell",
        for: indexPath) as! AddImageCollectionViewCell
        cell.myImage.image = photosArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width/3.2, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        print(images)
        imagePicker.dismiss(animated: true, completion: nil)
        self.photosArray = images
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
}


//MultiPart
extension ComplaintViewController {
    func upload(imagesKeyArray:[String], imagesDataArray: [Data], to url: String, params: [String: String]) {
        let frame = CGRect(x: self.view.frame.width / 2 , y: self.view.frame.height / 2, width: 0, height: 0)
         let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                             type: .ballScale)
        activityIndicatorView.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        activityIndicatorView.padding = 100
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in params {
                multiPart.append(value.data(using: .utf8)!, withName: key)
            }
            for i in 0..<imagesDataArray.count {
                multiPart.append(imagesDataArray[i], withName: imagesKeyArray[i],fileName: "personal.jpg", mimeType: "image/jpg")
            }
            
        }, to: url, method: .post, headers: nil)
        .responseJSON { (response) in
            switch response.result {
            case .success(let value):
                activityIndicatorView.stopAnimating()
                print(value)
                do {
                   let parsed = try JSONDecoder().decode(complaintResult.self, from: response.data!)
                    if parsed.success! == 1{
                        self.makeOkAlert(title: "", SubTitle: parsed.message ?? "", Image: UIImage())
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.makeOkAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
                    }
                }catch{
                   print (error)
                self.makeOkAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
                }
                
            case .failure(let error):
                activityIndicatorView.stopAnimating()
                print(error)
                self.makeOkAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
            }
        }
    }
}

struct complaintResult: Codable {
    let message: String?
    let success: Int?
}
