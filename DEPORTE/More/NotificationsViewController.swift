//
//  NotificationsViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyNotificationView: UIView!
    @IBOutlet weak var loginAlertLabel: UILabel!
    
    var userNotifications = Notify(){
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = NSLocalizedString("Notifications", comment: "")
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        emptyNotificationView.isHidden = true
        loginAlertLabel.isHidden = true
        
        if let ID = UserDefaults.standard.string(forKey: "clientID") {
            Services.getNotification(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
        }else{
            self.tableView.isHidden = true
            let alert = UIAlertController(title: "", message: NSLocalizedString("You have to login first", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Login", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                self.emptyNotificationView.isHidden = false
                self.loginAlertLabel.isHidden = false
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func handleResponse (data : Notify?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            userNotifications = data!
            print(userNotifications)
            if userNotifications.product?.count == 0 {
                tableView.isHidden = true
                emptyNotificationView.isHidden = false
            }else{
                tableView.isHidden = false
                emptyNotificationView.isHidden = true
            }
        }
    }

}
extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userNotifications.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        cell.notificationText.text = userNotifications.product?[indexPath.row].text
        cell.notificationDate.text = userNotifications.product?[indexPath.row].date
        return cell
    }
    
    
}
