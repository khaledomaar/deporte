//
//  MoreExtension.swift
//  DEPORTE
//
//  Created by mac on 15/12/2020.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation
import UIKit

extension MoreViewController{
    
    func logOut(){
        let alert = UIAlertController(title: "", message: NSLocalizedString("You want to logout", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            UserDefaults.standard.removeObject(forKey: "clientID")
            UserDefaults.standard.removeObject(forKey: "clientName")
            UserDefaults.standard.removeObject(forKey: "countryName")
            UserDefaults.standard.removeObject(forKey: "countryID")
            UserDefaults.standard.removeObject(forKey: "clientEmail")
            UserDefaults.standard.removeObject(forKey: "clientPhone")
            UserDefaults.standard.removeObject(forKey: "clientPassword")
            UserDefaults.standard.removeObject(forKey: "cartItems")
            UserDefaults.standard.removeObject(forKey: "loyaltySetting")
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(identifier: "CustomeTabBar") as! CustomeTabBar
            self.view.window?.rootViewController = vc
            vc.selectedIndex = 0
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func changeLanguage() {
        let refreshAlert = UIAlertController(title: NSLocalizedString("", comment: ""), message: NSLocalizedString("Choose language you want..", comment: ""), preferredStyle: UIAlertController.Style.alert)
        present(refreshAlert, animated: true, completion: nil)
        refreshAlert.addAction(UIAlertAction(title: "اللغة العربية", style: .default, handler: { (action: UIAlertAction!) in
            if UserDefaults.standard.string(forKey: "countryID") == "1"{
                L102Language.setAppleLAnguageTo(lang: "ar")
                LanguageManager.shared.setLanguage(language: .ar)
                APIs.Instance.setLangTO(lang: "ar")
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                self.restartApplication()
            }else{
                L102Language.setAppleLAnguageTo(lang: "ar-EG")
                LanguageManager.shared.setLanguage(language: .arEG)
                APIs.Instance.setLangTO(lang: "ar")
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                self.restartApplication()
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "English", style: .cancel, handler: { (action: UIAlertAction!) in
            L102Language.setAppleLAnguageTo(lang: "en")
            LanguageManager.shared.setLanguage(language: .en)
            APIs.Instance.setLangTO(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            self.restartApplication()
        }))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let userLogged = UserDefaults.standard.string(forKey: "clientID") {
            if UserDefaults.standard.string(forKey: "loyaltySetting") == "0" {
                switch indexPath.row {
                case 0:
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let intialViewController = sb.instantiateViewController(identifier: "CustomeTabBar") as! CustomeTabBar
                    intialViewController.selectedIndex = 0
                    self.view.window?.rootViewController = intialViewController
                case 1:
                    let sb = UIStoryboard(name: "Home", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "CategoriesViewController") as! CategoriesViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 2:
                    let sb = UIStoryboard(name: "Home", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "MyFavouriteViewController") as! MyFavouriteViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 3:
                    let sb = UIStoryboard(name: "Cart", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "CartViewController") as! CartViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 4:
                    let sb = UIStoryboard(name: "Orders", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "OrdersViewController") as! OrdersViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 5:
                    let sb = UIStoryboard(name: "More", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "AboutUsViewController") as! AboutUsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 6:
                    let sb = UIStoryboard(name: "More", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "contactUsViewController") as! contactUsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 7:
                    let sb = UIStoryboard(name: "Auth", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 8:
                    let sb = UIStoryboard(name: "More", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "ComplaintViewController") as! ComplaintViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 9:
                    logOut()
                case 10:
                    changeLanguage()
                default:
                    print("selected")
                }
            }else{
                switch indexPath.row {
                case 0:
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let intialViewController = sb.instantiateViewController(identifier: "CustomeTabBar") as! CustomeTabBar
                    intialViewController.selectedIndex = 0
                    self.view.window?.rootViewController = intialViewController
                case 1:
                    let sb = UIStoryboard(name: "Home", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "CategoriesViewController") as! CategoriesViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 2:
                    let sb = UIStoryboard(name: "Home", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "MyFavouriteViewController") as! MyFavouriteViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 3:
                    let sb = UIStoryboard(name: "Cart", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "CartViewController") as! CartViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 4:
                    let sb = UIStoryboard(name: "Orders", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "OrdersViewController") as! OrdersViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 5:
                    let sb = UIStoryboard(name: "Home", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "LoyaltyPointsViewController") as! LoyaltyPointsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 6:
                    let sb = UIStoryboard(name: "More", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "AboutUsViewController") as! AboutUsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 7:
                    let sb = UIStoryboard(name: "More", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "contactUsViewController") as! contactUsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 8:
                    let sb = UIStoryboard(name: "Auth", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 9:
                    let sb = UIStoryboard(name: "More", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "ComplaintViewController") as! ComplaintViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                case 10:
                    logOut()
                case 11:
                    changeLanguage()
                default:
                    print("selected")
                }
            }
        }else{
            switch indexPath.row {
            case 0:
                let sb = UIStoryboard(name: "Main", bundle: nil)
                let intialViewController = sb.instantiateViewController(identifier: "CustomeTabBar") as! CustomeTabBar
                intialViewController.selectedIndex = 0
                self.view.window?.rootViewController = intialViewController
            case 1:
                let sb = UIStoryboard(name: "Home", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "CategoriesViewController") as! CategoriesViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let sb = UIStoryboard(name: "More", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "AboutUsViewController") as! AboutUsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                let sb = UIStoryboard(name: "More", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "contactUsViewController") as! contactUsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 4:
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 5:
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "RegisterViewController") as! RegisterViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case 6:
                changeLanguage()
            default:
                print("selected")
            }
        }
    }
}
