//
//  AddImageCollectionViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class AddImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myImage: UIImageView!
    
}
