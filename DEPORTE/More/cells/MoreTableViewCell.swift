//
//  MoreTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var moreImage: UIImageView!
    @IBOutlet weak var moreTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if LanguageManager.shared.currentLanguage == .ar {
            menuContainer.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
