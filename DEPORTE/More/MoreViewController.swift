//
//  MoreViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var container: UIStackView!
    @IBOutlet weak var copyRightView: UIView!
    @IBOutlet weak var containerINStack: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var poweredByLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var versionView: UIView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var verifiedImage: UIImageView!
    @IBOutlet weak var checkUpdatesLabel: UILabel!
    @IBOutlet weak var chevronImage: UIImageView!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    var copyRightData = CopyRight(){
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = NSLocalizedString("More", comment: "")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        profileView.isHidden = true
        setTableViewTransformation()
        
        if UserDefaults.standard.string(forKey: "clientID") != nil {
            profileView.isHidden = false
            name.text = UserDefaults.standard.string(forKey: "clientName")
            phone.text = UserDefaults.standard.string(forKey: "clientPhone")
        }else{
            profileView.translatesAutoresizingMaskIntoConstraints = false
            profileView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Services.getSettings(vc: self, completionHundler: handleResponse(data:error:))
    }
    
    func setTableViewTransformation(){
        print(LanguageManager.shared.currentLanguage)
        if LanguageManager.shared.currentLanguage == .ar{
            tableView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            copyRightView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            container.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            companyNameLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            poweredByLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            versionLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            verifiedImage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            checkUpdatesLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            profileView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            chevronImage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }else if LanguageManager.shared.currentLanguage == .arEG{
            container.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            companyNameLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            poweredByLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            versionLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            verifiedImage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            checkUpdatesLabel.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            chevronImage.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }
    
    func handleResponse (data : CopyRight?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.copyRightData = data!
            print(copyRightData)
            companyNameLabel.text = copyRightData.product?.first?.copyright_name
            versionLabel.text = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
            let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
            print(appVersion)
            let DoubleAppVersion = Double(appVersion) ?? 5.0
            let apiVersion = Double(copyRightData.product?.first?.ios_version ?? "1.0") ?? 5.0
            if DoubleAppVersion != apiVersion {
                self.checkUpdatesLabel.text = "(Update Available)"
            }else{
                self.checkUpdatesLabel.text = ""
            }
            
        }
    }
    
    @IBAction func emcanButton(_ sender: Any) {
        guard let url = URL(string: copyRightData.product?.first?.copyright_link ?? "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func ProfileButton(_ sender: Any) {
        let sb = UIStoryboard(name: "Auth", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func restartApplication() {
        print(L102Language.currentAppleLanguage())
        if L102Language.currentAppleLanguage() == "en"{
            Bundle.setLanguage( "en")
        }else{
            if UserDefaults.standard.string(forKey: "countryID") == "1"{
                Bundle.setLanguage( "ar")
            } else {
                Bundle.setLanguage( "ar-EG")
            }
        }
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }

}
extension MoreViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.string(forKey: "clientID") != nil {
            if UserDefaults.standard.string(forKey: "loyaltySetting") == "0" {
                return 11
            }else{
                return 12
            }
        }else{
            return 7
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell", for: indexPath) as! MoreTableViewCell
        if let ID = UserDefaults.standard.string(forKey: "clientID") {
            if UserDefaults.standard.string(forKey: "loyaltySetting") == "0" {
                switch indexPath.row {
                case 0:
                    cell.moreImage.image = #imageLiteral(resourceName: "icons8-home")
                    cell.moreTitle.text = NSLocalizedString("Home", comment: "")
                case 1:
                    cell.moreImage.image = #imageLiteral(resourceName: "Page-1")
                    cell.moreTitle.text = NSLocalizedString("Categories", comment: "")
                case 2:
                    cell.moreImage.image = #imageLiteral(resourceName: "Layer_1")
                    cell.moreTitle.text = NSLocalizedString("Favourite", comment: "")
                case 3:
                    cell.moreImage.image = #imageLiteral(resourceName: "shopping-cart (1)")
                    cell.moreTitle.text = NSLocalizedString("Cart", comment: "")
                case 4:
                    cell.moreImage.image = #imageLiteral(resourceName: "Page-1")
                    cell.moreTitle.text = NSLocalizedString("Orders", comment: "")
                case 5:
                    cell.moreImage.image = #imageLiteral(resourceName: "about-us")
                    cell.moreTitle.text = NSLocalizedString("About us", comment: "")
                case 6:
                    cell.moreImage.image = #imageLiteral(resourceName: "13357228871579177879")
                    cell.moreTitle.text = NSLocalizedString("Contact us", comment: "")
                case 7:
                    cell.moreImage.image = #imageLiteral(resourceName: "user-")
                    cell.moreTitle.text = NSLocalizedString("Profile", comment: "")
                case 8:
                    cell.moreImage.image = #imageLiteral(resourceName: "suggestions and complains")
                    cell.moreTitle.text = NSLocalizedString("Suggestions and Complains", comment: "")
                case 9:
                    cell.moreImage.image = #imageLiteral(resourceName: "log-out")
                    cell.moreTitle.text = NSLocalizedString("Logout", comment: "")
                case 10:
                    cell.moreImage.image = #imageLiteral(resourceName: "language")
                    cell.moreTitle.text = NSLocalizedString("Change Language", comment: "")
                default:
                    print("")
                }
            }else{
                switch indexPath.row {
                case 0:
                    cell.moreImage.image = #imageLiteral(resourceName: "icons8-home")
                    cell.moreTitle.text = NSLocalizedString("Home", comment: "")
                case 1:
                    cell.moreImage.image = #imageLiteral(resourceName: "Page-1")
                    cell.moreTitle.text = NSLocalizedString("Categories", comment: "")
                case 2:
                    cell.moreImage.image = #imageLiteral(resourceName: "Layer_1")
                    cell.moreTitle.text = NSLocalizedString("Favourite", comment: "")
                case 3:
                    cell.moreImage.image = #imageLiteral(resourceName: "shopping-cart (1)")
                    cell.moreTitle.text = NSLocalizedString("Cart", comment: "")
                case 4:
                    cell.moreImage.image = #imageLiteral(resourceName: "Page-1")
                    cell.moreTitle.text = NSLocalizedString("Orders", comment: "")
                case 5:
                    cell.moreImage.image = #imageLiteral(resourceName: "credit-card (1)")
                    cell.moreTitle.text = NSLocalizedString("Loyalty Points", comment: "")
                case 6:
                    cell.moreImage.image = #imageLiteral(resourceName: "about-us")
                    cell.moreTitle.text = NSLocalizedString("About us", comment: "")
                case 7:
                    cell.moreImage.image = #imageLiteral(resourceName: "13357228871579177879")
                    cell.moreTitle.text = NSLocalizedString("Contact us", comment: "")
                case 8:
                    cell.moreImage.image = #imageLiteral(resourceName: "user-")
                    cell.moreTitle.text = NSLocalizedString("Profile", comment: "")
                case 9:
                    cell.moreImage.image = #imageLiteral(resourceName: "suggestions and complains")
                    cell.moreTitle.text = NSLocalizedString("Suggestions and Complains", comment: "")
                case 10:
                    cell.moreImage.image = #imageLiteral(resourceName: "log-out")
                    cell.moreTitle.text = NSLocalizedString("Logout", comment: "")
                case 11:
                    cell.moreImage.image = #imageLiteral(resourceName: "language")
                    cell.moreTitle.text = NSLocalizedString("Change Language", comment: "")
                default:
                    print("")
                }
            }
        }else{
            switch indexPath.row {
            case 0:
                cell.moreImage.image = #imageLiteral(resourceName: "icons8-home")
                cell.moreTitle.text = NSLocalizedString("Home", comment: "")
            case 1:
                cell.moreImage.image = #imageLiteral(resourceName: "Page-1")
                cell.moreTitle.text = NSLocalizedString("Categories", comment: "")
            case 2:
                cell.moreImage.image = #imageLiteral(resourceName: "about-us")
                cell.moreTitle.text = NSLocalizedString("About us", comment: "")
            case 3:
                cell.moreImage.image = #imageLiteral(resourceName: "13357228871579177879")
                cell.moreTitle.text = NSLocalizedString("Contact us", comment: "")
            case 4:
                cell.moreImage.image = #imageLiteral(resourceName: "log-out")
                cell.moreTitle.text = NSLocalizedString("Login", comment: "")
            case 5:
                cell.moreImage.image = #imageLiteral(resourceName: "log-out")
                cell.moreTitle.text = NSLocalizedString("Register", comment: "")
            case 6:
                cell.moreImage.image = #imageLiteral(resourceName: "language")
                cell.moreTitle.text = NSLocalizedString("Change Language", comment: "")
            default:
                print("")
            }
        }
        return cell
    }
    
}
