//
//  AboutUsViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {

    @IBOutlet weak var aboutUsContent: UITextView!
    @IBOutlet weak var aboutUsImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("About us", comment: "")
        Services.getAboutUs(vc: self, completionHundler: handleResponse(data:error:))

    }
    func handleResponse (data : AboutUs?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            aboutUsContent.text = data?.product?.first?.content
        }
    }

}
