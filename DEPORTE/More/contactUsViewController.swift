//
//  contactUsViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class contactUsViewController: BaseViewController {

    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var webLink: UILabel!
    
    var contact = Contact()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Contact us", comment: "")
        Services.getContact(vc: self, completionHundler: handleResponse(data:error:))
        
    }
    
    func handleResponse (data : Contact?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.contact = data!
            phone.text = data?.product?.first?.phone
            email.text = data?.product?.first?.email
            address.text = data?.product?.first?.address
            webLink.text = data?.product?.first?.website
        }
    }
    
    @IBAction func callNumber(_ sender: Any) {
        if let url = URL(string: "tel://\(contact.product?.first?.phone ?? "0552924091")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func facebookButton(_ sender: Any) {
        guard let url = URL(string: contact.product?.first?.facebook ?? "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func twitterButton(_ sender: Any) {
        guard let url = URL(string: contact.product?.first?.twitter ?? "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func instagramButton(_ sender: Any) {
        guard let url = URL(string: contact.product?.first?.instagram ?? "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func whatsApp(_ sender: Any) {
        guard let url = URL(string: contact.product?.first?.whatsapp ?? "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func snapChat(_ sender: Any) {
        guard let url = URL(string: contact.product?.first?.snapchat ?? "https://google.com") else { return }
        UIApplication.shared.open(url)
    }
    
}
