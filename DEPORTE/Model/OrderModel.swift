//
//  OrderModel.swift
//  DarElzain
//
//  Created by khaled omar on 5/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation

//MARK:-   Get Orders
struct Order: Decodable {
    var success: Int?
    var product: [OrderData]?
    var message: String?
}
struct OrderData: Decodable {
    
    var opened: Bool?
    var vat_added:String?
    var vat: String?
    var discount_value: String?
    var deliver_id: String?
    var client_id: String?
    var order_date: String?
    var order_status: String?
    var order_follow: String?
    var order_id: String?
    var discount_percentage: String?
    var total_price: String?
    var charge_cost: String?
    var total_price_without_charge: String?
    var net_price: String?
    var items: [ItemsData]?

}
struct ItemsData: Decodable {
    var cart_id: String?
    var remove_id: String?
    var sub_category_id: String?
    var sub_category_name: String?
    var sub_category_desc: String?
    var sub_category_images: [String]?
    var size_name: String?
    var size_price: String?
    var quantity: String?
    var price: String?
    var color_name: String?
}
