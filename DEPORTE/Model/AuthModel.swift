//
//  AuthModel.swift
//  DarElzain
//
//  Created by khaled omar on 5/11/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation

//MARK:-   Login

struct Login: Decodable {
    var success: Int?
    var product: [LoginData]?
    var message: String?
}
struct LoginData: Decodable {
    var client_id: String?
    var client_name: String?
    var client_password: String?
    var client_email: String?
    var client_phone: String?
    var date: String?
    var country_data: CountryData?
}
//MARK:-   Register

struct Register: Decodable {
    var success: Int?
    var product: [RegisterData]?
    var message: String?
}
struct RegisterData: Decodable {
    var client_id: Int?
    var client_name: String?
    var client_password: String?
    var client_email: String?
    var client_phone: String?
    var country_data: CountryData?
}
//MARK:-   EditProfile

struct EditProfile: Decodable {
    var success: Int?
    var product: [EditProfileData]?
    var message: String?
}
struct EditProfileData: Decodable {
    var client_id: String?
    var client_name: String?
    var client_password: String?
    var client_email: String?
    var client_phone: String?
//    var client_image: String?
    var date: String?

}
//MARK:-   EditProfile

struct NumberExist: Decodable {
    var success: Int?
    var exist: Int?
    var client_id: String?
    var message: String?
}
