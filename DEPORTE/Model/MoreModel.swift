//
//  MoreModel.swift
//  DarElzain
//
//  Created by khaled omar on 5/12/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation

struct AboutUs: Decodable {
    var success: Int?
    var product: [AboutUsData]?
    var message: String?
}
struct AboutUsData: Decodable {
    var id: String?
    var image: String?
    var content: String?
}
//MARK:- Get Contact us
struct Contact: Decodable {
    var success: Int?
    var product: [ContactData]?
    var message: String?
}
struct ContactData: Decodable {
    var id: String?
    var address: String?
    var phone: String?
    var mobile: String?
    var email: String?
    var instagram: String?
    var twitter: String?
    var facebook: String?
    var website: String?
    var whatsapp: String?
    var snapchat: String?

}
//MARK:- Get Notification
struct Notify: Decodable {
    var success: Int?
    var product: [NotificationData]?
    var message: String?
}
struct NotificationData: Decodable {
    var id: String?
    var text: String?
    var date: String?
    var type: String?
    var text_id: String?
}
//MARK:- Get Setting api
struct CopyRight: Decodable {
    var success: Int?
    var product: [CopyRightData]?
    var message: String?
}
struct CopyRightData: Decodable {
    var ios_version: String?
    var android_version: String?
    var copyright_name: String?
    var copyright_link: String?
}
