//
//  HomeModel.swift
//  Layalina
//
//  Created by Mac on 3/24/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import Foundation

struct Home: Decodable {
    var success: Int?
    var slider: [SubCatData]?
    var parent_categories: [ParentCatData]?
    var most_request: [SubCatData]?
    var latest: [SubCatData]?
    var loyalty_setting: String?
    var message: String?
}

//MARK:-   Parent Categories
struct ParentCat: Decodable {
    var success: Int?
    var product: [ParentCatData]?
    var message: String?
}
struct ParentCatData: Decodable {
    var parent_category_id: String?
    var parent_category_name: String?
    var parent_category_desc: String?
    var parent_category_image: String?
    var type: String?
    var arrangement: String?
}
//MARK:-  Sub Parent Categories
struct SubParent: Decodable {
    var success: Int?
    var data: [SubParentData]?
    var message: String?
}
struct SubParentData: Decodable {
    var id: String?
    var parent_category_id: String?
    var name: String?
    var description: String?
    var image: String?
    var display: String?
    var date: String?
    
}
//MARK:-   Sub Categories
struct SubCategories: Decodable {
    var success: Int?
    var product: [SubCatData]?
    var message: String?
}
struct SubCatData: Decodable {
    var sub_category_id: String?
    var sub_category_name: String?
    var sub_category_desc: String?
    var sub_category_image: [String]?
    var parent_category_id: String?
    var evaluate: Double?
    var sub_category_fav: Int?
    var slider_image: String?
    var sizes: [SizesData]?
    var colors: [ColorsData]?
    var isProduct: Bool?
}
//MARK:-   Sub Categories Sizes
struct SubCatSize: Decodable {
    var success: Int?
    var product: [SizesData]?
    var message: String?
}
struct SizesData: Decodable {
    var sub_category_size_price_id: String?
    var sub_category_size_name: String?
    var sub_category_size_price: String?
    var sub_category_id: String?
    var has_discount: String?
    var discount_percentage: String?
    var sub_category_size_price_after_discount: String?
}
struct ColorsData: Decodable {
    var id: String?
    var name: String?
    var date_added: String?
}
//MARK:-   Add Comment
struct AddComment: Decodable {
    var success: Int?
    var product: [AddCommentData]?
    var message: String?
}
struct AddCommentData: Decodable {
    var client_id: String?
    var sub_category_id: String?
    var comment: String?
    var rate: String?
}
//MARK:-   Get Comment
struct GetComment: Decodable {
    var success: Int?
    var product: [GetCommentData]?
    var message: String?
}
struct GetCommentData: Decodable {
    var comment_id: String?
    var client_id: String?
    var comment: String?
    var rate: String?
    var date: String?
    var client_name: String?
}
//MARK:-   Edit Comment
//  Delete comment
//  Delete Favourite
struct lightResponse: Decodable {
    var success: Int?
    var product: [lightResponseData]?
    var message: String?
}
struct lightResponseData: Decodable {
    
}
//MARK:-   Add Favourite
struct AddFavourite: Decodable {
    var success: Int?
    var product: [AddFavouriteData]?
    var message: String?
}
struct AddFavouriteData: Decodable {
    var fav_id: Int?
    var client_id: String?
    var sub_category_id: String?
}
//MARK:-   Get Favourite
struct GetFavourite: Decodable {
    var success: Int?
    var product: [SubCatData]?
    var message: String?
}
//MARK:-   Get Addtions
struct Addtions: Decodable {
    var success: Int?
    var product: [AddtionsData]?
    var message: String?
}
struct AddtionsData: Decodable {
    var addition_id: String?
    var addition_name: String?
    var addition_price: String?
    
}
//MARK:-   Get Removes
struct Removes: Decodable {
    var success: Int?
    var product: [RemovesData]?
    var message: String?
}
struct RemovesData: Decodable {
    var remove_id: String?
    var remove_title: String?
    
}
//MARK:-   Get Search Result
struct Search: Decodable {
    var success: Int?
    var product: [SubCatData]?
    var message: String?
}
//MARK:-   Get loyalty points
struct loyaltyPoints: Decodable {
    var message: String?
    var points_text: String?
    var total_amount: String?
    var client_points_show_pop: String?
    var client_points: String?
    var success: Int?
}

//MARK:-   Get Sort and Filter Options
struct SortFilterOptions: Decodable {
    var success: Int?
    var sorts: [OptionsData]?
    var filters: [OptionsData]?
    var countires: [CountryData]?
    var message: String?

}
struct CountryData: Decodable {
    var id: String?
    var name: String?
    var flag: String?
    var code: String?
    var display: String?
    var charge: String?
    var min_order: String?
    var date_added: String?
}
struct OptionsData: Decodable {
    var opened: Bool?
    var id: String?
    var name: String?
    var date_added: String?
    var filter_options: [FilterOptions]?

}
struct FilterOptions: Decodable{
    var id: String?
    var name: String?
    var date_added: String?
}
