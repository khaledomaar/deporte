//
//  CommentsModel.swift
//  CocoDip
//
//  Created by khaled omar on 5/28/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation
//MARK:- Get Comment
struct Message: Decodable {
    var success: Int?
    var product: [MessageData]?
    var message: String?
}
struct MessageData: Decodable {
    var content: String?
    var is_read: String?
    var type: String?
    var date: String?
}
//MARK:- Get Complaints

struct GetComplaints: Decodable {
    var success: Int?
    var product: [ComplaintsData]?
    var message: String?
}
struct ComplaintsData: Decodable {
    var complaint_id: String?
    var content: String?
    var title: String?
    var date: String?
    var type: String?
    var messages: [MessageDetails]?
    var message_id: Int?
}
struct MessageDetails: Decodable {
    var complaint_id: String?
    var content: String?
    var title: String?
    var date: String?
    var type: String?
    var is_read: String?
}
//MARK:- Get Messages

struct GetMessages: Decodable {
    var success: Int?
    var product: [MessageDetails]?
    var message: String?
}
//MARK:- add Message

struct AddMessage: Decodable {
    var success: Int?
    var product: [ComplaintsData]?
    var message: String?
}
