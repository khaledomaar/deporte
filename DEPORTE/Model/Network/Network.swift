//
//  Network.swift
//  Syanat
//
//  Created by Omar Adel on 12/14/19.
//  Copyright © 2019 z510. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView

class networkRequest{
    class func Request(vc: UIViewController ,url : String ,method: HTTPMethod, parameters: [String : Any]?, headers: [String: String]?, completionHundler: @escaping (AFDataResponse<Any>?,ErrorHandler?)->Void){
        print (url)
        print (parameters)
        let frame = CGRect(x: vc.view.frame.width / 2 , y: vc.view.frame.height / 2, width: 0, height: 0)
         let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                             type: .ballScale)
        activityIndicatorView.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        activityIndicatorView.padding = 100
        vc.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        AF.request(url, method: method, parameters: parameters ?? [String: Any](),encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse) in
            switch(response.result) {
            case .success(let value):
                activityIndicatorView.stopAnimating()
                print(value)
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message ?? "errorerrorerror")
                        completionHundler(nil, err)
                    }catch{
                        print("errorrrrelse")
                        print(error)
                    }
                }else{
                    print(response.data!)
                    completionHundler(response,nil)
                }
            case .failure(let error):
                activityIndicatorView.stopAnimating()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                print(lockString)
                print(error)
                completionHundler(nil,nil)
                break
            }
        }
    }
    class func BodyRequest(vc: UIViewController ,url : String ,method: HTTPMethod, parameters: [String : Any]?, headers: [String: String]?, completionHundler: @escaping (AFDataResponse<Any>?,ErrorHandler?)->Void){
        print (url)
        print (parameters)
        let frame = CGRect(x: vc.view.frame.width / 2 , y: vc.view.frame.height / 2, width: 0, height: 0)
         let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                             type: .ballScale)
        activityIndicatorView.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        activityIndicatorView.padding = 100
        vc.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        AF.request(url, method: method, parameters: parameters ?? [String: Any](),encoding: JSONEncoding.default, headers: nil).responseJSON { (response:AFDataResponse) in
            switch(response.result) {
            case .success(let value):
                activityIndicatorView.stopAnimating()
                print(value)
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message ?? "errorerrorerror")
                        completionHundler(nil, err)
                    }catch{
                        print("errorrrrelse")
                        print(error)
                    }
                }else{
                    print(response.data!)
                    completionHundler(response,nil)
                }
            case .failure(_):
                activityIndicatorView.stopAnimating()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                print(lockString)
                completionHundler(nil,nil)
                break
            }
        }
    }
}
