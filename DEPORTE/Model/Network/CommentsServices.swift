//
//  CommentsServices.swift
//  DarElzain
//
//  Created by Mac on 5/21/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation
import UIKit
extension Services {
    //MARK: -
    class func getComents(clientID: String, typeID: String,vc : UIViewController , completionHundler: @escaping (Message?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getCommentByType(clientID: clientID, typeID: typeID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Message?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getComplaints(clientID: String, vc : UIViewController , completionHundler: @escaping (GetComplaints?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getUserComplaints(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetComplaints?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getmagsByComplaintID(complaintID: String, vc : UIViewController , completionHundler: @escaping (GetMessages?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getMessagesByComplaintID(complaintID: complaintID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetMessages?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func addMessage(clientId: String, content: String, complaint_id: String, vc : UIViewController , completionHundler: @escaping (AddMessage?,String?) -> Void){
        
        let par = ["client_id" : clientId , "content" : content ,"complaint_id" : complaint_id , "message_type_id" : "1" ]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.addMessageToChat(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AddMessage?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
}
