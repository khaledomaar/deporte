//
//  CartServices.swift
//  Anhar
//
//  Created by Mac on 4/15/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import Foundation
import UIKit

extension Services {
    //MARK: -
    class func getDeliverWayes(vc : UIViewController , completionHundler: @escaping (DeliverWays?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getDeliverWayes(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(DeliverWays?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getBranches(vc : UIViewController , completionHundler: @escaping (GetBranches?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getAllBranches(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetBranches?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func addAdress(clientPhone: String, lat: String, lang: String, countryID: String, region_id: String, block: String, road: String, building: String, flat_number: String, note: String, client_id: String, vc : UIViewController , completionHundler: @escaping (AddAddress?,String?) -> Void){
        
        let par = ["lat" : lat , "lang" : lang , "country_id" : countryID , "region_id" : region_id, "block" : block, "road" : road , "building" : building, "flat_number" : flat_number, "client_phone" : clientPhone , "note" : note, "client_id" : client_id]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.addClientAddress(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AddAddress?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getRegion(vc : UIViewController , completionHundler: @escaping (Region?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getAllRegions(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Region?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getAddresses(clientID: String, vc : UIViewController , completionHundler: @escaping (GetAddress?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getClientAddresses(clientId: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetAddress?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func addToCart(subCatId: String, sizeID: String, colorID: String, quatity: String, clientID: String, note: String, type: String, vc : UIViewController , completionHundler: @escaping (AddCart?,String?) -> Void){
        
        let par = ["sub_category_id" : subCatId , "size_id" : sizeID , "color_id" : colorID, "quantity" : quatity, "client_id" : clientID ,  "note" : note, "type" : type , "lang" : APIs.Instance.lang]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.addCart(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AddCart?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getCart(clientID: String, addressID: String, vc : UIViewController , completionHundler: @escaping (GetCart?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getClientCart(clientID: clientID, addressID: addressID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetCart?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func editCart(clientID: String, quantity: String, cartID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.editCartItem(clientID: clientID, quantity: quantity, cartID: cartID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func DeletedishFromCart(clientID: String, cartID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.deleteOneItemFromCart(clientID: clientID, cartID: cartID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func deleteAddress(addressID: String, clientID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.deleteClientAddress(addressID: addressID, clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func deleteAllCart(clientID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.deleteAllItemsCart(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func editAddress(addressID: String, clientPhone: String, lat: String, lang: String, countryID: String, regionName: String, block: String, road: String, building: String, flat_number: String, note: String, client_id: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        
        let par = ["client_address_id" : addressID , "lat" : lat, "country_id" : countryID, "region_id" : regionName, "block" : block, "road" : road , "building" : building, "flat_number" : flat_number, "client_phone" : clientPhone , "note" : note, "client_id" : client_id, "lang" : APIs.Instance.lang]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.editClientAddresses(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func confirmOrder(payment: String, deliverID: String, clientID: String, addressID: String, note: String, cartIDs: String, branchId: String, usePoints: String, vc : UIViewController , completionHundler: @escaping (ConfirmOrder?,String?) -> Void){
        
        let par = ["payment" : payment , "deliver_id" : deliverID , "client_id" : clientID , "client_address_id" : addressID, "cart_id" : cartIDs, "use_points" : usePoints  ,"lang" : APIs.Instance.lang , "mobile_type" : "ios" , "branch_id" : branchId, "note" : note , "mobile_version" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? "" ]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.confirmClientOrder(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(ConfirmOrder?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getPaymentMethods(vc : UIViewController , completionHundler: @escaping (PaymentMethods?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getPaymentMethod(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(PaymentMethods?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func available(vc : UIViewController , completionHundler: @escaping (AvailableOrder?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.checkAvailable(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AvailableOrder?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func checkOrder(clientID: String, clientAddressID: String, cartID: String, orderID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.checkOrders(clientID: clientID, clientAddressID: clientAddressID, cartID: cartID, orderID: orderID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func checkCart(cartID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.checkCart(cartID: cartID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func addPayment(client_id: String, order_id: String, value: String, result: String, payment_id: String, branch_id: String ,vc : UIViewController , completionHundler: @escaping (ConfirmOrder?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.addPayment(client_id: client_id, order_id: order_id, value: value, result: result, payment_id: payment_id, branch_id: branch_id), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(ConfirmOrder?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func paymentResult(client_id: String, session_id: String, vc : UIViewController , completionHundler: @escaping (CreditPaymentResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.paymentResult(client_id: client_id, session_id: session_id), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(CreditPaymentResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
}
