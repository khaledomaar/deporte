//
//  Services.swift
//  ELMenu
//
//  Created by Mac on 2/11/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

class Services {
    //MARK: -
    class func getHome(clientID: String, vc : UIViewController , completionHundler: @escaping (Home?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getHomePage(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Home?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getParentCats(vc : UIViewController , completionHundler: @escaping (ParentCat?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getParentCat(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(ParentCat?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getSubParent(parentID: String, vc : UIViewController , completionHundler: @escaping (SubParent?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getSubParentcategory(parentID: parentID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(SubParent?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    class func getSubCat(parentCategoryId: String, subParentID: String, clientId: String, vc : UIViewController , completionHundler: @escaping
        (SubCategories?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getSubCats(parentCategoryId: parentCategoryId, subParentId: subParentID, clientId: clientId), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(SubCategories?.self,from:(response?.data)!)
                           // if success == true return data and error = nil
                           // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getSubCatSizes(subCatId: String, vc : UIViewController , completionHundler: @escaping (SubCatSize?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getSubCatSize(subCatID: subCatId), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(SubCatSize?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func addComment(clientId: String, subCatId: String, comment: String, rate: String, vc : UIViewController , completionHundler: @escaping (AddComment?,String?) -> Void){
           
        let par = ["client_id" : clientId , "sub_category_id" : subCatId ,"comment" : comment , "rate" : rate ]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.addUserComment(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
                response , error in
            if response == nil && error == nil{
                    completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AddComment?.self,from:(response?.data)!)
                              // if success == true return data and error = nil
                              // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                        }
                    }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getComments(subCatId: String, vc : UIViewController , completionHundler: @escaping (GetComment?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getComment(subCatID: subCatId), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetComment?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func editComment(commentID: String, comment: String, rate: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
           
        let par = ["comment_id" : commentID , "comment" : comment ,"rate" : rate ]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.EditUserComment(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
                response , error in
            if response == nil && error == nil{
                    completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                              // if success == true return data and error = nil
                              // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                        }
                    }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func deleteComments(commentID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.deleteUserComment(commentID: commentID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func addToFavourites(subCatID: String, clientID:String, vc : UIViewController , completionHundler: @escaping (AddFavourite?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.addToFavourite(subCatID: subCatID, clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AddFavourite?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func deleteFromFav(subCatID: String, clientID:String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.deleteFavourite(subCatID: subCatID, clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getFavourites(clientID:String, vc : UIViewController , completionHundler: @escaping (GetFavourite?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getClientFavourites(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(GetFavourite?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getAddtions(parentCatID: String, clientID:String, vc : UIViewController , completionHundler: @escaping (Addtions?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getAddtions(clientID: clientID, parentCatID: parentCatID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Addtions?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getRemoves(parentCatID: String, clientID:String, vc : UIViewController , completionHundler: @escaping (Removes?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getRemove(clientID: clientID, parentCatID: parentCatID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Removes?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getSearch(key: String, clientID:String, vc : UIViewController , completionHundler: @escaping (Search?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.search(clientID: clientID, key: key), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Search?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getLoyaltyPoints(cartID: String, clientID:String, addressID: String, vc : UIViewController , completionHundler: @escaping (loyaltyPoints?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.loyaltyPoints(clientID: clientID, cartID: cartID, addressID: addressID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(loyaltyPoints?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func lookUp(vc : UIViewController , completionHundler: @escaping (SortFilterOptions?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getSortandFilterOptions(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(SortFilterOptions?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func sort(clientID: String, key: String, vc : UIViewController , completionHundler: @escaping (SubCategories?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getSortResults(clientID: clientID, key: key), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(SubCategories?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func filter(clientID: String, key: String, keyValue: String, vc : UIViewController , completionHundler: @escaping (SubCategories?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getFilterResults(clientID: clientID, key: key, keyValue: keyValue), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(SubCategories?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
}
