//
//  MoreServices.swift
//  DarElzain
//
//  Created by khaled omar on 5/12/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation
import UIKit

extension Services {
    //MARK: -
    class func getAboutUs(vc : UIViewController , completionHundler: @escaping (AboutUs?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getAbout(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(AboutUs?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getContact(vc : UIViewController , completionHundler: @escaping (Contact?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getContactUs(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Contact?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getNotification(clientID: String, vc : UIViewController , completionHundler: @escaping (Notify?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getUserNotifications(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(Notify?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func getSettings(vc : UIViewController , completionHundler: @escaping (CopyRight?,String?) -> Void){
        networkRequest.Request(vc: vc, url: APIs.Instance.getSetting(), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
            response , error in
            if response == nil && error == nil{
                completionHundler(nil,nil)
            }else{
                if error == nil{
                    do {
                        let ParsedResult = try JSONDecoder().decode(CopyRight?.self,from:(response?.data)!)
                        // if success == true return data and error = nil
                        // if success == false retrun data with nil and error with String
                        (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                    }catch{
                        print("errorrrr catcchhchchchc")
                        print (error)
                    }
                }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
}
