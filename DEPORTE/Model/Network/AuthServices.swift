//
//  AuthServices.swift
//  Anhar
//
//  Created by Mac on 4/15/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import Foundation
import UIKit

extension Services{
    //MARK: -
    class func userLogin(clientPassword: String, clientPhone: String, deviceToken: String, type: String, phoneNumber: String, vc : UIViewController , completionHundler: @escaping (Login?,String?) -> Void){
        
        let par = ["client_password" : clientPassword , "client_phone" : clientPhone ,"device_token" : deviceToken , "type" : type , "phone_number": phoneNumber , "lang" : APIs.Instance.lang]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.LoginUser(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
               response , error in
               if response == nil && error == nil{
                   completionHundler(nil,nil)
               }else{
                   if error == nil{
                       do {
                        let ParsedResult = try JSONDecoder().decode(Login?.self,from:(response?.data)!)
                           // if success == true return data and error = nil
                           // if success == false retrun data with nil and error with String
                           (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                       }catch{
                           print("errorrrr catcchhchchchc")
                           print (error)
                       }
                   }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: - Register

    class func register(clientName: String, clientPassword: String, clientPhone: String, deviceToken: String, type: String, countryID: String, vc : UIViewController , completionHundler: @escaping (Register?,String?) -> Void){
        
        let par = ["client_name" : clientName , "client_password" : clientPassword, "client_phone" : clientPhone, "device_token" : deviceToken, "type" : type, "country_id" : countryID , "lang" : APIs.Instance.lang]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.RegisterNewUer(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
               response , error in
               if response == nil && error == nil{
                   completionHundler(nil,nil)
               }else{
                   if error == nil{
                       do {
                           let ParsedResult = try JSONDecoder().decode(Register?.self,from:(response?.data)!)
                           // if success == true return data and error = nil
                           // if success == false retrun data with nil and error with String
                           (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                       }catch{
                           print("errorrrr catcchhchchchc")
                           print (error)
                       }
                   }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func editProfile(clientId: String, clientName: String, clientPassword: String, clientPhone: String, vc : UIViewController , completionHundler: @escaping (EditProfile?,String?) -> Void){
        
        let par = ["client_id" : clientId , "client_name" : clientName , "client_password" : clientPassword , "client_phone" : clientPhone, "lang" : APIs.Instance.lang]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.editClient(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
               response , error in
               if response == nil && error == nil{
                   completionHundler(nil,nil)
               }else{
                   if error == nil{
                       do {
                           let ParsedResult = try JSONDecoder().decode(EditProfile?.self,from:(response?.data)!)
                           // if success == true return data and error = nil
                           // if success == false retrun data with nil and error with String
                           (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                       }catch{
                           print("errorrrr catcchhchchchc")
                           print (error)
                       }
                   }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func recoverPassworD(phoneNum: String, clientPassword: String, clientID: String, vc : UIViewController , completionHundler: @escaping (lightResponse?,String?) -> Void){
        
        let par = ["lang" : APIs.Instance.lang , "phone" : phoneNum ,"password" : clientPassword , "client_id" : clientID ]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.recoverPassword(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
               response , error in
               if response == nil && error == nil{
                   completionHundler(nil,nil)
               }else{
                   if error == nil{
                       do {
                        let ParsedResult = try JSONDecoder().decode(lightResponse?.self,from:(response?.data)!)
                           // if success == true return data and error = nil
                           // if success == false retrun data with nil and error with String
                           (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                       }catch{
                           print("errorrrr catcchhchchchc")
                           print (error)
                       }
                   }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
    //MARK: -
    class func checkNumberExist(clientNum: String, vc : UIViewController , completionHundler: @escaping (NumberExist?,String?) -> Void){
        
        let par = ["client_phone" : clientNum, "lang" : APIs.Instance.lang]
        
        networkRequest.BodyRequest(vc: vc, url: APIs.Instance.checkClientPhoneExist(), method: .post, parameters: par, headers: APIs.Instance.getHeader()){
               response , error in
               if response == nil && error == nil{
                   completionHundler(nil,nil)
               }else{
                   if error == nil{
                       do {
                        let ParsedResult = try JSONDecoder().decode(NumberExist?.self,from:(response?.data)!)
                           // if success == true return data and error = nil
                           // if success == false retrun data with nil and error with String
                           (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                       }catch{
                           print("errorrrr catcchhchchchc")
                           print (error)
                       }
                   }else{
                    completionHundler(nil, error?.message)
                }
            }
        }
    }
}
