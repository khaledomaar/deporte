//
//  ViewController.swift
//  Rezerva
//
//  Created by Mohamed Nawar on 5/4/19.
//


import Foundation

class APIs {
    static let Instance = APIs()
    private init() {}
    
    public func getHeader() -> [String: String]{
        let header = [
            "Accept" : "application/json" , "Content-Type": "charset=utf-8"]
        return header
    }
    var lang = L102Language.currentAppleLanguage()
    private let url = "http://deporte-bh.com/system/api/"
    
    //MARK:- Home Apis
    public func setLangTO(lang: String){  
        self.lang = lang
    }
    public func getHomePage(clientID: String) -> String{
        return url + "main_page.php?lang=\(lang)&client_id=\(clientID)"
    }
    public func getParentCat() -> String{
        return url + "get-parent-categories.php?lang=\(lang)"
    }
    public func getSubParentcategory(parentID: String) -> String{
        return url + "get-sub-parent-byparent-id.php?lang=\(lang)&parent_id=\(parentID)"
    }
    public func getSubCats(parentCategoryId: String, subParentId: String, clientId: String) -> String{
        return url + "get-sub-categories.php?lang=\(lang)&sub_parent_id=\(subParentId)&parent_category_id=\(parentCategoryId)&client_id=\(clientId)"
    }
    public func getSubCatSize(subCatID: String) -> String{
        return url + "get-sub-categories-size-prices.php?sub_category_id=\(subCatID)&lang=\(lang)"
    }
    public func addUserComment() -> String{
        return url + "add-comment.php"
    }
    public func getComment(subCatID: String) -> String{
        return url + "get-sub-category-comments.php?sub_category_id=\(subCatID)"
    }
    public func EditUserComment() -> String{
        return url + "edit-comment.php"
    }
    public func deleteUserComment(commentID: String) -> String{
        return url + "delete-comment.php?comment_id=\(commentID)"
    }
    public func addToFavourite(subCatID: String, clientID: String) -> String{
        return url + "add-fav.php?sub_category_id=\(subCatID)&client_id=\(clientID)"
    }
    public func deleteFavourite(subCatID: String, clientID: String) -> String{
        return url + "delete-fav.php?sub_category_id=\(subCatID)&client_id=\(clientID)"
    }
    public func getClientFavourites(clientID: String) -> String{
        return url + "get-client-fav.php?client_id=\(clientID)"
    }
    public func getAddtions(clientID: String, parentCatID: String) -> String{
        return url + "get-addition-prices.php?parent_category_id=\(parentCatID)&client_id=\(clientID)&lang=\(lang)"
    }
    public func getRemove(clientID: String, parentCatID: String) -> String{
        return url + "get-removes.php?parent_category_id=\(parentCatID)&client_id=\(clientID)&lang=\(lang)"
    }
    public func search(clientID: String, key: String) -> String{
        return url + "search-subCategories.php?lang=\(lang)&key=\(key)&client_id=\(clientID)"
    }
    public func loyaltyPoints(clientID: String, cartID: String, addressID: String) -> String{
        return url + "get-client-points.php?client_id=\(clientID)&lang=\(lang)&cart_id=\(cartID)&client_address_id=\(addressID)"
    }
    public func getSortandFilterOptions() -> String{
        return url + "lookUp.php?lang=\(lang)"
    }
    public func getSortResults(clientID: String, key: String) -> String{
        return url + "sub-category-sort.php?lang=\(lang)&client_id=\(clientID)&key=\(key)"
    }
    public func getFilterResults(clientID: String, key: String, keyValue: String) -> String{
        return url + "sub-category-filter.php?lang=\(lang)&client_id=\(clientID)&key=\(key)&key_value=\(keyValue)"
    }

    //MARK:- Cart Apis
    
    public func getDeliverWayes() -> String{
        return url + "get-delivered_way.php?lang=\(lang)"
    }
    public func getAllBranches() -> String{
        return url + "get-branches.php?lang=\(lang)"
    }
    public func addClientAddress() -> String{
        return url + "add-client-address.php"
    }
    public func deleteClientAddress(addressID: String, clientID: String) -> String{
        return url + "delete-client-address.php?client_address_id=\(addressID)&client_id=\(clientID)"
    }
    public func getAllRegions() -> String{
        return url + "get-all-regions.php?lang=\(lang)"
    }
    public func getClientAddresses(clientId: String) -> String{
        return url + "get-client-addresses.php?client_id=\(clientId)&lang=\(lang)"
    }
    public func editClientAddresses() -> String{
        return url + "edit-client-address.php"
    }
    public func addCart() -> String{
        return url + "add-to-cart.php"
    }
    public func editCartItem(clientID: String, quantity: String, cartID: String) -> String{
        return url + "edit-cart.php?cart_id=\(cartID)&quantity=\(quantity)&client_id=\(clientID)&lang=\(lang)"
    }
    public func getClientCart(clientID: String,  addressID: String) -> String{
        return url + "get-cart.php?client_id=\(clientID)&lang=\(lang)&address_id=\(addressID)"
    }
    public func deleteOneItemFromCart(clientID: String, cartID:String) -> String{
        return url + "delete-cart.php?cart_id=\(cartID)&client_id=\(clientID)"
    }
    public func deleteAllItemsCart(clientID: String) -> String{
        return url + "empty-cart.php?lang=\(lang)&client_id=\(clientID)"
    }
    public func getPaymentMethod() -> String{
        return url + "get-payment-methods.php?lang=\(lang)"
    }
    public func confirmClientOrder() -> String{
        return url + "confirm-order.php"
    }
    public func checkAvailable() -> String{
        return url + "avaliable.php?lang=\(lang)"
    }
    public func checkOrders(clientID: String, clientAddressID: String, cartID: String, orderID: String) -> String{
        return url + "check-order.php?client_id=\(clientID)&client_address_id=\(clientAddressID)&cart_id=\(cartID)&order_id=\(orderID)&lang=\(lang)"
    }
    public func checkCart(cartID: String) -> String{
        return url + "check-subcategory-exist.php?lang=\(lang)&cart_id=\(cartID)"
    }
    //MARK:- Auth Apis
    
    public func LoginUser() -> String{
        return url + "login.php"
    }
    public func RegisterNewUer() -> String{
        return url + "add-client.php"
    }
    public func editClient() -> String{
        return url + "edit-client.php"
    }
    public func recoverPassword() -> String{
        return url + "update_pass.php"
    }
    public func checkClientPhoneExist() -> String{
        return url + "check-number-exist.php"
    }
    //MARK:- More Apis
    public func getAbout() -> String{
        return url + "get-about.php?lang=\(lang)"
    }
    public func getContactUs() -> String{
           return url + "get-contact.php?lang=\(lang)"
    }
    public func getUserNotifications(clientID: String) -> String{
           return url + "get-notify.php?client_id=\(clientID)"
    }
    public func getSetting() -> String{
        return url + "get-setting.php"
    }
    //MARK:- Order Apis
    public func getClientCurrentOrders(clientID: String) -> String{
        return url + "my-current-orders.php?client_id=\(clientID)&lang=\(lang)"
    }
    public func getLastOrders(clientID: String) -> String{
        return url + "my-last-orders.php?client_id=\(clientID)&lang=\(lang)"
    }
    
    //MARK: -
    public func addCompliant() -> String{
        return url + "add-complaint.php"
    }
    //MARK:- CommentsApi
    public func getCommentByType(clientID: String, typeID: String) -> String{
        return url + "get-messages.php?lang=\(lang)&message_type_id=\(typeID)&client_id=\(clientID)"
    }
    public func getUserComplaints(clientID: String) -> String{
        return url + "get-complaints.php?lang=\(lang)&client_id=\(clientID)"
    }
    public func getMessagesByComplaintID(complaintID: String) -> String{
        return url + "get-message-by-complain-id.php?complaint_id=\(complaintID)"
    }
    public func addMessageToChat() -> String{
        return url + "add-message.php"
    }
}
extension APIs {
    //MARK: - credit
    public func paymentResult(client_id: String, session_id: String) -> String{
        return "http://deporte-bh.com/credit/payment-result.php?client_id=\(client_id)&session_id=\(session_id)"
    }
//MARK: - depit
    public func addPayment(client_id: String, order_id: String, value: String, result: String, payment_id: String, branch_id: String) -> String{
        return url + "add-payment.php?client_id=\(client_id)&order_id=\(order_id)&value=\(value)&result=\(result)&payment_id=\(payment_id)&branch_id=\(branch_id)"
    }
}
