//
//  OrderServices.swift
//  DarElzain
//
//  Created by khaled omar on 5/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation
import UIKit

extension Services {
//MARK: -
    class func getCurrentOrder(clientID: String, vc : UIViewController , completionHundler: @escaping (Order?,String?) -> Void){
    networkRequest.Request(vc: vc, url: APIs.Instance.getClientCurrentOrders(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
        response , error in
        if response == nil && error == nil{
            completionHundler(nil,nil)
        }else{
            if error == nil{
                do {
                    let ParsedResult = try JSONDecoder().decode(Order?.self,from:(response?.data)!)
                    // if success == true return data and error = nil
                    // if success == false retrun data with nil and error with String
                    (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                }catch{
                    print("errorrrr catcchhchchchc")
                    print (error)
                }
            }else{
                completionHundler(nil, error?.message)
            }
        }
      }
    }
    //MARK: -
    class func getPreviosOrder(clientID: String, vc : UIViewController , completionHundler: @escaping (Order?,String?) -> Void){
    networkRequest.Request(vc: vc, url: APIs.Instance.getLastOrders(clientID: clientID), method: .get, parameters: nil, headers: APIs.Instance.getHeader()){
        response , error in
        if response == nil && error == nil{
            completionHundler(nil,nil)
        }else{
            if error == nil{
                do {
                    let ParsedResult = try JSONDecoder().decode(Order?.self,from:(response?.data)!)
                    // if success == true return data and error = nil
                    // if success == false retrun data with nil and error with String
                    (ParsedResult?.success == 1) ? completionHundler(ParsedResult,nil) : completionHundler(nil,ParsedResult?.message as? String)
                }catch{
                    print("errorrrr catcchhchchchc")
                    print (error)
                }
            }else{
                completionHundler(nil, error?.message)
            }
        }
      }
    }
}
