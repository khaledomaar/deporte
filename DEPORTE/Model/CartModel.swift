//
//  CartModel.swift
//  DarElzain
//
//  Created by khaled omar on 5/11/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation

struct DeliverWays: Decodable {
    var success: Int?
    var product: [DeliverWaysData]?
    var message: String?
}
struct DeliverWaysData: Decodable {
    var deliver_id: String?
    var display: String?
    var name: String?

}
//MARK:-   Get Branches
struct GetBranches: Decodable {
    var success: Int?
    var product: [GetBranchesData]?
    var message: String?
}
struct GetBranchesData: Decodable {
    var branch_id: String?
    var name: String?
}
//MARK:-   Add Address
struct AddAddress: Decodable {
    var success: Int?
    var product: [AddAddressData]?
    var message: String?
}
struct AddAddressData: Decodable {
    var client_address_id: Int?
    var lat: String?
    var lang: String?
    var region_id: String?
    var region_name: String?
    var charge: String?
    var min_order: String?
    var block: String?
    var road: String?
    var building: String?
    var flat_number: String?
    var client_phone: String?
    var note: String?
    var client_id: String?
}
//MARK:-   Get Region
struct Region: Decodable {
    var success: Int?
    var product: [RegionData]?
    var message: String?
}
struct RegionData: Decodable {
    var region_id: String?
    var region_name: String?
    var city_id: String?
    var charge: String?
    var min_order: String?
    var order_period: String?
}
//MARK:-   Get Address
struct GetAddress: Decodable {
    var success: Int?
    var product: [GetAddressData]?
    var message: String?
}
struct GetAddressData: Decodable {
    var client_address_id: String?
    var lat: String?
    var lang: String?
    var region_id: String?
    var region_name: String?
    var country_name: String?
    var country_id: String?
    var charge: String?
    var min_order: String?
    var block: String?
    var road: String?
    var building: String?
    var flat_number: String?
    var client_phone: String?
    var note: String?
    var client_id: String?
}
//MARK:-   Add To Cart
struct AddCart: Decodable {
    var success: Int?
    var product: [AddCartData]?
    var message: String?
}
struct AddCartData: Decodable {
    var cart_id: Int?
    var total: Double?
    var client_id: String?
}
//MARK:-   Get Cart
struct GetCart: Decodable {
    var success: Int?
    var product: [GetCartData]?
    var message: String?
}
struct GetCartData: Decodable {
    var cart_id: String?
    var sub_category_id: String?
    var remove_id: String?
    var sub_category_name: String?
    var sub_category_desc: String?
    var sub_category_images: [String]?
    var color_name: String?
    var size_id: String?
    var size_name: String?
    var size_price: String?
    var addition_id: String?
    var quantity: String?
    var price: String?
    var client_id: String?
    var total_amount: String?
    var check_discount: String?
    var discount_percentage: Double?
    var total_amount_after_disc: String?
    var vat: String?
    var vat_value: String?
    var discount_value: String?
    var charge: String?
    var summary: String?
}
//MARK:-   Add To Cart
struct ConfirmOrder: Decodable {
    var success: Int?
    var not_exist: Int?
    var product: [ConfirmOrderData]?
    var message: String?
}
struct ConfirmOrderData: Decodable {
    var order_id: Int?
}
//MARK:-   get payment methods
struct PaymentMethods: Decodable {
    var success: Int?
    var product: [PaymentData]?
    var message: String?
}
struct PaymentData: Decodable {
    var deliver_id: String?
    var display: String?
    var name: String?

}
//MARK:-   Check Available
struct AvailableOrder: Decodable {
    var success: Int?
    var product: [AvailableData]?
    var message: String?
}
struct AvailableData: Decodable {
    var accept_orders: String?
    var discount: String?
    var discount_percentage: Int?
}
//MARK:-   Check Available

struct CreditPaymentResponse : Codable{
    var success: Int?
    var product: [CreditPaymentData]?
    var message: String?
}

struct CreditPaymentData : Codable{
    var client_id: String?
    var session_id: String?
    var value: String?
}
