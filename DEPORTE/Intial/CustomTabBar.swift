//
//  CustomeTabBar.swift
//  Syanat
//
//  Created by Omar Adel on 12/1/19.
//  Copyright © 2019 z510. All rights reserved.
//

import UIKit

class CustomeTabBar: UITabBarController {
    
    var token : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        tabBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        setupTabBar()
        self.tabBar.frame.size.height = 40
        self.tabBar.layer.masksToBounds = true
    }
        
    func setupTabBar(){
        
        let firstStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let first = firstStoryBoard.instantiateViewController(withIdentifier: "HomeNav")
        let home = #imageLiteral(resourceName: "internet")
        let homeSelected = #imageLiteral(resourceName: "internet-1")
        let homeImage = home.withRenderingMode(.alwaysOriginal)
        let homeSelectedImage = homeSelected.withRenderingMode(.alwaysOriginal)
        first.tabBarItem = UITabBarItem(title: NSLocalizedString("", comment: ""), image: homeImage, selectedImage: homeSelectedImage)
        
        let thirdStoryBoard = UIStoryboard(name: "More", bundle: nil)
        let second = thirdStoryBoard.instantiateViewController(withIdentifier: "NotificationNav")
        let notifySelected = #imageLiteral(resourceName: "bell-1")
        let notifyImage = notifySelected.withRenderingMode(.alwaysOriginal)
        second.tabBarItem = UITabBarItem(title: NSLocalizedString("", comment: ""), image: #imageLiteral(resourceName: "bell"), selectedImage: notifyImage )
        
        let fourthStoryboard = UIStoryboard(name: "Comments", bundle: nil)
        let third = fourthStoryboard.instantiateViewController(withIdentifier: "CommentsNav")
        let commentSelected = #imageLiteral(resourceName: "chat-bubbles-with-ellipsis-1")
        let commentImage = commentSelected.withRenderingMode(.alwaysOriginal)
        third.tabBarItem = UITabBarItem(title: NSLocalizedString("", comment: ""), image: #imageLiteral(resourceName: "chat-bubbles-with-ellipsis"), selectedImage: commentImage)
        
        let fifthStoryboard = UIStoryboard(name: "More", bundle: nil)
        let fourth = fifthStoryboard.instantiateViewController(withIdentifier: "MoreNav")
        let more = #imageLiteral(resourceName: "XMLID_22_")
        let moreSelected = #imageLiteral(resourceName: "XMLID_22_-1")
        let moreImage = more.withRenderingMode(.alwaysOriginal)
        let moreSelectedImage = moreSelected.withRenderingMode(.alwaysOriginal)
        fourth.tabBarItem = UITabBarItem(title: NSLocalizedString("", comment: ""), image: moreImage, selectedImage: moreSelectedImage)

        
        setBadge(first: first, second: second, third: third, fourth: fourth)
        tabBarController?.tabBar.frame.size.height = 100
    }
    
    func setBadge(first: UIViewController, second: UIViewController ,third: UIViewController , fourth: UIViewController ){
        
        self.viewControllers = [first , second , third , fourth]
    }

}

