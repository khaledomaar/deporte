//
//  BaseViewController.swift
//  Layalina
//
//  Created by Mac on 3/21/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    let cartBT = BadgeButton()
    
    var cartButtonClickable = true {
        didSet{
            cartBT.isEnabled = cartButtonClickable
        }
    }
    var withOutNavBar = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        let searchButton = UIButton()
        searchButton.setImage(UIImage(named: "search-1"), for: .normal)
        searchButton.contentMode = .scaleAspectFit
        searchButton.addTarget(self, action: #selector(openSearchVc), for: .touchUpInside)
        let searchBarButton = UIBarButtonItem(customView: searchButton)
        navigationItem.rightBarButtonItem = searchBarButton
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.barStyle = .black
        navigationItem.leftItemsSupplementBackButton = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        let numOfCartItem = UserDefaults.standard.string(forKey: "cartItems") ?? "0"
        addleftBarButtonItemWithBadge(itemvalue: numOfCartItem)
        
    }
    @objc func openSearchVc (){
        let secStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = secStoryboard.instantiateViewController(identifier: "SearchViewController") as! SearchViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func clickOnCartButton() {
        if let ID = UserDefaults.standard.string(forKey: "clientID") {
            let cartStoryboard = UIStoryboard(name: "Cart", bundle: nil)
            let vc = cartStoryboard.instantiateViewController(identifier: "CartViewController") as! CartViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let alert = UIAlertController(title: "", message: NSLocalizedString("You have to login first", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Login", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        
        if withOutNavBar == true {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
        }else{
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationController?.navigationBar.barStyle = .black
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
            navigationItem.leftItemsSupplementBackButton = true
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        let numOfCartItem = UserDefaults.standard.string(forKey: "cartItems") ?? "0"
        addleftBarButtonItemWithBadge(itemvalue: numOfCartItem)
        
    }
    
    func addleftBarButtonItemWithBadge(itemvalue: String) {
        cartBT.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartBT.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cartBT.setImage(UIImage(named: "shoppingCart")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartBT.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartBT.badge = itemvalue
        cartBT.contentMode = .scaleAspectFill
        cartBT.addTarget(self, action: #selector(clickOnCartButton), for: .touchUpInside)
        let cartLeftBarButton = UIBarButtonItem(customView: cartBT)
        navigationItem.leftBarButtonItem = cartLeftBarButton
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
