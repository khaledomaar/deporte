//
//  CategoriesCollectionViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    
}
