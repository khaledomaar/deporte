//
//  OptionsForFilterTableViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 10/5/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class OptionsForFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var filterOptionName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
