//
//  ProductDetailsTableViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import ImageSlideshow

class ProductDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var sliderImage: ImageSlideshow!
    @IBOutlet weak var sizeName: UILabel!
    @IBOutlet weak var colorName: UILabel!
    @IBOutlet weak var isFavouriteImage: UIImageView!
    @IBOutlet weak var discountPriceContainerView: UIView!
    @IBOutlet weak var priceBeforeDiscount: UILabel!
    
    
    var pushSizesVcCallBack: (()->())?
    var pushColorsVcCallBack: (()->())?
    var favouriteButtonCallBack: (()->())?
    var goToGalleryCallBack: ((_ sliderShow: ImageSlideshow)->())?
    var pushToPresenytImageCallBack: ((_ selectedImage: String)->())?
    
    var sliders = [String](){
        didSet{
            configureSlider(sliderView: sliderImage)
        }
    }
    var currentPage = 0

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureSlider(sliderView: ImageSlideshow) {
        var sliderImages = [SDWebImageSource]()
        for slider in self.sliders{
            guard let imageSource = SDWebImageSource(urlString: slider.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") else {return}
            sliderImages.append(imageSource)
        }
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.lightGray
        pageIndicator.pageIndicatorTintColor = #colorLiteral(red: 0.9770210385, green: 0.7992018461, blue: 0.07289702445, alpha: 1)
        sliderView.pageIndicator = pageIndicator
        
        sliderView.currentPageChanged = { more in
            self.currentPage = (sliderView.pageIndicator?.page ?? 0)
        }
        
        sliderView.contentScaleMode = .scaleToFill
        sliderView.setImageInputs(sliderImages)
        sliderView.slideshowInterval = 2
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        sliderView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func didTap(sliderShow: ImageSlideshow) {
//        pushToPresenytImageCallBack?(sliders[currentPage])
        goToGalleryCallBack?(self.sliderImage)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func sizeButton(_ sender: Any) {
        pushSizesVcCallBack?()
    }
    @IBAction func colorButton(_ sender: Any) {
        pushColorsVcCallBack?()
    }
    @IBAction func favouriteButton(_ sender: Any) {
        favouriteButtonCallBack?()
    }
    
}
