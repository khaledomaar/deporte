//
//  ProductsInHomeCollectionViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ProductsInHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var priceBeforeDisc: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var discountPriceContainerView: UIView!
    
}
