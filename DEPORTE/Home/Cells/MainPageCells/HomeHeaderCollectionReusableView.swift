//
//  HomeHeaderCollectionReusableView.swift
//  DEPORTE
//
//  Created by khaled omar on 10/3/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import ImageSlideshow

class HomeHeaderCollectionReusableView: UICollectionReusableView {
 
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sliderImage: ImageSlideshow!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sliderProductName: UILabel!
    var selectedProductInsliderCallBack: ((_: SubCatData)->())?
    var pushSubParentVCCallBack: ((_ parentID: String, _ parentImage: String, _ parentName: String)->())?
    var sliders = [SubCatData](){
        didSet{
            configureSlider(sliderView: sliderImage)
        }
    }
    var parentCat = [ParentCatData](){
        didSet{
            collectionView.reloadData()
        }
    }
    var currentPage = 0 {
        didSet{
            setProductNameOnSlider(page: currentPage)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func configureSlider(sliderView: ImageSlideshow) {
        var sliderImages = [SDWebImageSource]()
        for slider in self.sliders{
            guard let imageSource = SDWebImageSource(urlString: slider.slider_image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") else {return}
            sliderImages.append(imageSource)
        }
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.lightGray
        pageIndicator.pageIndicatorTintColor = #colorLiteral(red: 0.9770210385, green: 0.7992018461, blue: 0.07289702445, alpha: 1)
        sliderView.pageIndicator = pageIndicator
        sliderView.contentScaleMode = .scaleToFill
        sliderView.setImageInputs(sliderImages)
        sliderView.slideshowInterval = 3
        sliderView.currentPageChanged = { page in
            self.currentPage = (sliderView.pageIndicator?.page ?? 0)
        }
        if sliders.count != 0 {
            sliderProductName.text = sliders[sliderView.pageIndicator?.page ?? 0].sub_category_name
        }
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        sliderView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func didTap(){
        if sliders[currentPage].isProduct == true {
            let seletedItem = sliders[currentPage]
            selectedProductInsliderCallBack?(seletedItem)
        }
    }
    
    func setProductNameOnSlider(page: Int) {
        if sliders[page].isProduct == true  {
            sliderProductName.isHidden = false
            sliderProductName.text = sliders[page].sub_category_name
        }else{
            sliderProductName.isHidden = true
        }
    }

}
extension HomeHeaderCollectionReusableView: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return parentCat.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SectionsInHomeCollectionViewCell", for: indexPath) as! SectionsInHomeCollectionViewCell
        cell.name.text = parentCat[indexPath.item].parent_category_name
        cell.parentCatImage.sd_setImage(with: URL(string: parentCat[indexPath.item].parent_category_image ?? ""))
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let parentID = parentCat[indexPath.item].parent_category_id ?? ""
        let parentImage = parentCat[indexPath.item].parent_category_image ?? ""
        let parentName = parentCat[indexPath.item].parent_category_name ?? ""
        pushSubParentVCCallBack?(parentID, parentImage, parentName)
    }
}
