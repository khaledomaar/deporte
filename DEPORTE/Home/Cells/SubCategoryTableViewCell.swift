//
//  SubCategoryTableViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/16/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class SubCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if L102Language.currentAppleLanguage() == "ar" {
            arrowImageView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
