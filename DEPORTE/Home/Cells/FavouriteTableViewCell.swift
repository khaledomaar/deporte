//
//  FavouriteTableViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/15/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class FavouriteTableViewCell: UITableViewCell {

    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var dishDesc: UILabel!
    @IBOutlet weak var discountPriceContainerView: UIView!
    @IBOutlet weak var priceBeforeDiscount: UILabel!
    
    var favouriteCallBack: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favouriteButton(_ sender: Any) {
        self.favouriteCallBack?()
    }
    
}
