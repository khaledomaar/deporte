//
//  ReviewsCollectionViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import Cosmos

class ReviewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewContent: UILabel!
    @IBOutlet weak var deleteRateView: UIView!
    
    var commentID = ""
        
    @IBAction func deleteRateBtn(_ sender: Any) {
        let dic = ["commentID": commentID]
        NotificationCenter.default.post(name: NSNotification.Name("deleteRate"), object: nil, userInfo: dic)
    }
}
