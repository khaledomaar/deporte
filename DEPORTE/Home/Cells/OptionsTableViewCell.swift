//
//  OptionsTableViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var optionName: UILabel!
    @IBOutlet weak var checkedImage: UIImageView!
    @IBOutlet weak var sizePrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
