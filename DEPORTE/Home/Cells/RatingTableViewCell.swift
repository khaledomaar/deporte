//
//  RatingTableViewCell.swift
//  DEPORTE
//
//  Created by khaled omar on 9/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyRatesLabel: UILabel!
    
    var reviewData = GetComment(){
        didSet{
            collectionView.reloadData()
        }
    }
    var pushAddRateVcCallBack: (()->())?
    var pushEditRateVcCallBack: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func addRateButton(_ sender: Any) {
        pushAddRateVcCallBack?()
    }
    
}
extension RatingTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviewData.product?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewsCollectionViewCell", for: indexPath) as! ReviewsCollectionViewCell
        cell.userName.text = reviewData.product?[indexPath.item].client_name
        cell.reviewContent.text = reviewData.product?[indexPath.item].comment
        cell.commentID = reviewData.product?[indexPath.item].comment_id ?? ""
        cell.ratingView.rating = Double(reviewData.product?[indexPath.item].rate ?? "") ?? 0
        let ID = UserDefaults.standard.string(forKey: "clientID")
        if ID == reviewData.product?[indexPath.item].client_id {
            cell.deleteRateView.isHidden = false
        }else{
            cell.deleteRateView.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ID = UserDefaults.standard.string(forKey: "clientID")
        if ID == reviewData.product?[indexPath.item].client_id {
            pushEditRateVcCallBack?()
        }else{
            
        }
    }
}
