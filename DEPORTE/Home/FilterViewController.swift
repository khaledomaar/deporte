//
//  FilterViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/27/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class FilterViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var options = SortFilterOptions(){
        didSet{
            tableView.reloadData()
        }
    }
    var selectedOptionCallBack: ((_ key: String, _ keyValue: String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        Services.lookUp(vc: self, completionHundler: handleResponse(data:error:))
        
    }
    func handleResponse (data : SortFilterOptions?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.options = data!
            print(options)
        }
    }
}
extension FilterViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return options.filters?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if options.filters?[section].opened == true {
            return (options.filters?[section].filter_options?.count ?? 0) + 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let filterCell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell", for: indexPath) as! OptionsTableViewCell
        let optionsFilterCell = tableView.dequeueReusableCell(withIdentifier: "OptionsForFilterTableViewCell", for: indexPath) as! OptionsForFilterTableViewCell
        if indexPath.row == 0 {
            filterCell.optionName.text = options.filters?[indexPath.section].name
            return filterCell
        }else{
            optionsFilterCell.filterOptionName.text = options.filters?[indexPath.section].filter_options?[indexPath.row - 1].name
            return optionsFilterCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if options.filters?[indexPath.section].opened == true{
                options.filters?[indexPath.section].opened = false
                tableView.reloadData()
            }else{
                options.filters?[indexPath.section].opened = true
                tableView.reloadData()
            }
        }else{
            let key = options.filters?[indexPath.section].id ?? ""
            let keyValue = options.filters?[indexPath.section].filter_options?[indexPath.row - 1].id ?? ""
            self.selectedOptionCallBack?(key, keyValue)
            navigationController?.popViewController(animated: true)
        }
    }
}
