//
//  ColorsViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ColorsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var colors = [ColorsData]()
    var selectedColor = 0
    var selectedColorId = ""
    var selectedColorName = ""
    var selectedColorCallBack: ((_ ColorID: String, _ ColorName: String, _ selectedColor: Int)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func pickColorButton(_ sender: Any) {
        self.selectedColorCallBack?(selectedColorId, selectedColorName, selectedColor)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension ColorsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell", for: indexPath) as! OptionsTableViewCell
        cell.optionName.text = colors[indexPath.row].name
        if indexPath.row == selectedColor{
            cell.checkedImage.image = UIImage(named: "check")
            selectedColorId = colors[indexPath.row].id ?? ""
            selectedColorName = colors[indexPath.row].name ?? ""
        }else{
            cell.checkedImage.image = UIImage()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedColor = indexPath.row
        self.tableView.reloadData()
    }
}
