//
//  ExtensionProductDetailsVC.swift
//  DEPORTE
//
//  Created by khaled omar on 9/19/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import Foundation
import UIKit

extension ProductDetailsViewController{
    
    func handleAddCartResponse (data : AddCart?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            makeOkAlert(title: NSLocalizedString("Product added to cart successfully", comment: ""), SubTitle: "", Image: UIImage())
//            AllCartData.addtions = [AddtionsData]()
            self.sizeID = ""
            self.colorID = ""
            self.tableView.reloadData()
            let storyBoard = UIStoryboard(name: "Cart", bundle: nil)
            let vc = storyBoard.instantiateViewController(identifier: "CartViewController") as! CartViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func handleAddFavResponse (data : AddFavourite?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            makeOkAlert(title: NSLocalizedString("You have add this product to favourite", comment: ""), SubTitle: "", Image: UIImage())
            self.isFavouriteImage = UIImage(named: "heart22")
            self.tableView.reloadData()
        }
    }
    func handleDeleteFavResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            makeOkAlert(title: NSLocalizedString("You Deleted this product from favourite", comment: ""), SubTitle: "", Image: UIImage())
            self.isFavouriteImage = UIImage(named: "heart")
            self.tableView.reloadData()
        }
    }
    func handleGetCommentsResponse (data : GetComment?, error : String?){
    if error != nil {
        // lw fe error
        self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
    }else if error == nil && data == nil {
        // lw mfesh wla data wla error
        self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
    }else {
        // lw fe data w mfes herror
        comments = data!
        print(comments)
        }
    }
    func handleDeleteCommentResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            deleteComment = data!
            print(deleteComment)
            makeOkAlert(title: NSLocalizedString("Deleted", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
}
