//
//  MyFavouriteViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class MyFavouriteViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyFavouriteView: UIView!
    
    var getFav = GetFavourite(){
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Favourite", comment: "")

        tableView.delegate = self
        tableView.dataSource = self
        
        emptyFavouriteView.isHidden = true
       
    }
    override func viewWillAppear(_ animated: Bool) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getFavourites(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
        print(ID)
    }
    
    func handleResponse (data : GetFavourite?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            getFav = data!
            print(getFav)
            if getFav.product?.count == 0 {
                tableView.isHidden = true
                emptyFavouriteView.isHidden = false
            }else{
                tableView.isHidden = false
                emptyFavouriteView.isHidden = true
            }
        }
    }
    func handleDeleteFavResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            makeOkAlert(title:  NSLocalizedString("You Deleted this product from favourite", comment: ""), SubTitle: "", Image: UIImage())
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getFavourites(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
        }
    }
}
extension MyFavouriteViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getFav.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteTableViewCell", for: indexPath) as! FavouriteTableViewCell
        cell.dishName.text = getFav.product?[indexPath.row].sub_category_name
        cell.dishDesc.text = (getFav.product?[indexPath.row].sizes?.first?.sub_category_size_price_after_discount ?? "") + " BHD"
        if getFav.product?[indexPath.row].sizes?.first?.has_discount == "0"{
            cell.discountPriceContainerView.isHidden = true
        }else{
            cell.discountPriceContainerView.isHidden = false
            cell.priceBeforeDiscount.text = (getFav.product?[indexPath.row].sizes?.first?.sub_category_size_price ?? "") + " BHD"
        }
        cell.dishImage.sd_setImage(with: URL(string: getFav.product?[indexPath.row].sub_category_image?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        
        cell.favouriteCallBack = {
            let alert = UIAlertController(title: "", message: NSLocalizedString("Are you sure you want to delete this product from favourite", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                let userID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                let subCatId = self.getFav.product?[indexPath.row].sub_category_id ?? ""
                Services.deleteFromFav(subCatID: subCatId, clientID: userID, vc: self, completionHundler: self.handleDeleteFavResponse(data:error:))
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailsViewController") as! ProductDetailsViewController
        vc.product = getFav.product?[indexPath.row] ?? SubCatData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
