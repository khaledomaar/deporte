//
//  HomeViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var home = Home(){
        didSet{
            collectionView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = NSLocalizedString("Home", comment: "")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewLayOut()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getHome(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
    func collectionViewLayOut() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
        layout.itemSize = CGSize(width: (self.view.frame.width / 2) - 10  , height: 240.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    func handleResponse (data : Home?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.home = data!
            UserDefaults.standard.setValue(data?.loyalty_setting, forKey: "loyaltySetting")
            print(home)
        }
    }
    private func configureHeaderCell(cell: HomeHeaderCollectionReusableView, indexPath: IndexPath) {
        if indexPath.section == 1{
            if home.most_request?.count == 0{
                cell.titleLabel.text = NSLocalizedString("", comment: "")
            }else{
                cell.titleLabel.text = NSLocalizedString("Most Request", comment: "")
            }
        }else{
            if home.latest?.count == 0{
                cell.titleLabel.text = NSLocalizedString("", comment: "")
            }else{
                cell.titleLabel.text = NSLocalizedString("Latest", comment: "")
            }
        }
        cell.sliders = home.slider ?? []
        cell.parentCat = home.parent_categories ?? []
        cell.pushSubParentVCCallBack = { parentID ,parentImage, parentName in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoriesViewController") as! SubCategoriesViewController
            vc.parentID = parentID
            vc.catTile = parentName
            vc.parentCatImage = parentImage
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.selectedProductInsliderCallBack = { selectedItem in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
            vc.product = selectedItem
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return home.latest?.count ?? 0
        }else{
            return home.most_request?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsInHomeCollectionViewCell", for: indexPath) as! ProductsInHomeCollectionViewCell
        if indexPath.section == 1{
            cell.name.text = home.most_request?[indexPath.item].sub_category_name
            cell.price.text = (home.most_request?[indexPath.item].sizes?.first?.sub_category_size_price_after_discount ?? "") + " BHD"
            if home.most_request?[indexPath.item].sizes?.first?.has_discount == "0"{
                cell.discountPriceContainerView.isHidden = true
            }else{
                cell.discountPriceContainerView.isHidden = false
                cell.priceBeforeDisc.text = (home.most_request?[indexPath.item].sizes?.first?.sub_category_size_price ?? "") + " BHD"
            }
            cell.productImage.sd_setImage(with: URL(string: home.most_request?[indexPath.item].sub_category_image?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
            
            return cell
        }else{
            cell.name.text = home.latest?[indexPath.item].sub_category_name
            cell.price.text = (home.latest?[indexPath.item].sizes?.first?.sub_category_size_price_after_discount ?? "") + " BHD"
            if home.latest?[indexPath.item].sizes?.first?.has_discount == "0"{
                cell.discountPriceContainerView.isHidden = true
            }else{
                cell.discountPriceContainerView.isHidden = false
                cell.priceBeforeDisc.text = (home.latest?[indexPath.item].sizes?.first?.sub_category_size_price ?? "") + " BHD"
            }
            cell.productImage.sd_setImage(with: URL(string: home.latest?[indexPath.item].sub_category_image?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let header = collectionView.dequeueReusableHeader(indexPath: indexPath) as HomeHeaderCollectionReusableView
            configureHeaderCell(cell: header, indexPath: indexPath)
            return header
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        if indexPath.section == 1{
            vc.product = home.most_request?[indexPath.item] ?? SubCatData()
        }else{
            vc.product = home.latest?[indexPath.item] ?? SubCatData()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension UICollectionView {
    func dequeueReusableHeader<Header: UICollectionReusableView>(indexPath: IndexPath) -> Header {
        guard
            let header = dequeueReusableSupplementaryView(
                ofKind: UICollectionView.elementKindSectionHeader,
                withReuseIdentifier: "\(Header.self)",
                for: indexPath
            ) as? Header
        else {
            return Header()
        }
        return header
    }
}
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        if section == 0{
            return CGSize(width: collectionView.frame.width, height: 465)
        }else{
            return CGSize(width: collectionView.frame.width, height: 50)
        }
    }
}
