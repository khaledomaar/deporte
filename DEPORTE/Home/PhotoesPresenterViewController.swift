//
//  PhotoesPresenterViewController.swift
//  Ednuvo
//
//  Created by mac on 11/15/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class PhotoesPresenterViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var imageToPresent: UIImageView!
    
    var selectedImage = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scroll.delegate = self
        imageToPresent.sd_setImage(with: URL(string: selectedImage), placeholderImage: UIImage(named: "default"))

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageToPresent
    }
    
}
