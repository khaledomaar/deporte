//
//  SearchViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/22/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    var serachResult = Search(){
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Search", comment: "")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        searchText.delegate = self
        searchText.returnKeyType = UIReturnKeyType.search
        searchText.setAlignment(lang: LanguageManager.shared.currentLanguage)
        
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getSearch(key: "", clientID: ID, vc: self, completionHundler: handleSearchResponse(data:error:))
    }
    
    func handleSearchResponse (data : Search?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.serachResult = data!
            print(serachResult)
        }
    }
}
extension SearchViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serachResult.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
        cell.dishName.text = serachResult.product?[indexPath.row].sub_category_name
        cell.dishDesc.text = (serachResult.product?[indexPath.row].sizes?.first?.sub_category_size_price ?? "") + " BHD"
        cell.dishImage.sd_setImage(with: URL(string: serachResult.product?[indexPath.row].sub_category_image?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        
        return cell
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if searchText.text != "" {
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getSearch(key: searchText.text ?? "", clientID: ID, vc: self, completionHundler: handleSearchResponse(data:error:))
        }else{
        }
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        vc.product = self.serachResult.product?[indexPath.row] ?? SubCatData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
