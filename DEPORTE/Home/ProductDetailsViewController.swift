//
//  ProductDetailsViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ProductDetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var comments = GetComment(){
        didSet{
            tableView.reloadData()
        }
    }
    var deleteComment = lightResponse(){
        didSet{
            tableView.reloadData()
            Services.getComments(subCatId: product.sub_category_id ?? "", vc: self, completionHundler: handleGetCommentsResponse(data:error:))
        }
    }
    var product = SubCatData()
    var sizeID = ""
    var sizeName = ""
    var selectedSizeIndex = 0{
        didSet{
            tableView.reloadData()
        }
    }
    var colorID = ""
    var colorName = ""
    var selectedColorIndex = 0
    var isFavouriteImage = UIImage(named: "heart")
    var screenTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = product.sub_category_name
        
        tableView.delegate = self
        tableView.dataSource = self
        Services.getComments(subCatId: product.sub_category_id ?? "", vc: self, completionHundler: handleGetCommentsResponse(data:error:))
        NotificationCenter.default.addObserver(self, selector: #selector(deleteRate(notification:)) , name: NSNotification.Name("deleteRate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showRate(notification:)), name: Notification.Name("showRate") , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showRate(notification:)), name: Notification.Name("showEditRate") , object: nil)
        
        handleHeartImage()
    }
    @objc func showRate(notification: NSNotification) {
        Services.getComments(subCatId: product.sub_category_id ?? "", vc: self, completionHundler: handleGetCommentsResponse(data:error:))
    }
    @objc func deleteRate(notification: NSNotification) {
        let commentID = notification.userInfo?["commentID"] as? String ?? ""
        let alert = UIAlertController(title: "", message: NSLocalizedString("Are you sure you want to delete this rate", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            Services.deleteComments(commentID: commentID, vc: self, completionHundler: self.handleDeleteCommentResponse(data:error:))
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleHeartImage(){
        if product.sub_category_fav == 0{
            isFavouriteImage = UIImage(named: "heart")
        }else{
            isFavouriteImage = UIImage(named: "heart22")
        }
    }
    func configureFavourites(cell: ProductDetailsTableViewCell){
        if let ID = UserDefaults.standard.string(forKey: "clientID") {
            if cell.isFavouriteImage.image == UIImage(named: "heart"){
                // do add to fav
                let subCatId = self.product.sub_category_id ?? ""
                Services.addToFavourites(subCatID: subCatId, clientID: ID, vc: self, completionHundler: self.handleAddFavResponse(data:error:))
            }else{
                // do delete from fav
                let alert = UIAlertController(title: "", message: NSLocalizedString("Are you sure you want to delete this product from favourite", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    let userID = UserDefaults.standard.string(forKey: "clientID") ?? ""
                    let subCatId = self.product.sub_category_id ?? ""
                    Services.deleteFromFav(subCatID: subCatId, clientID: userID, vc: self, completionHundler: self.handleDeleteFavResponse(data:error:))
                    
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let alert = UIAlertController(title: "", message: NSLocalizedString(" You have to login first", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("login", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    let sb = UIStoryboard(name: "Auth", bundle: nil)
                    let vc = sb.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }))
                alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
    }
    func configureAddRate(){
        if UserDefaults.standard.string(forKey: "clientID") != nil {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddCommentViewController") as! AddCommentViewController
            vc.subCatID = self.product.sub_category_id ?? ""
            self.present(vc, animated: true)
        }else{
            let alert = UIAlertController(title: "", message: NSLocalizedString("You have to login first", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Login", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func addToCartButton(_ sender: Any) {
        if let ID = UserDefaults.standard.string(forKey: "clientID") {
            if self.sizeName == "" || self.colorName == ""{
                makeOkAlert(title: NSLocalizedString("You have to pick Size & Color first", comment: ""), SubTitle: "", Image: UIImage())
            }else{
                Services.addToCart(subCatId: product.sub_category_id ?? "", sizeID: sizeID, colorID: colorID, quatity: "1", clientID: ID, note: "", type: "ios", vc: self, completionHundler: handleAddCartResponse(data:error:))
            }
            
        }else{
            let alert = UIAlertController(title: "", message: NSLocalizedString(" You have to login first", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("login", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                let sb = UIStoryboard(name: "Auth", bundle: nil)
                let vc = sb.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
extension ProductDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsTableViewCell", for: indexPath) as! ProductDetailsTableViewCell
        let ratingCell = tableView.dequeueReusableCell(withIdentifier: "RatingTableViewCell", for: indexPath) as! RatingTableViewCell
        if indexPath.section == 0{
            detailsCell.name.text = product.sub_category_name
            detailsCell.price.text = (product.sizes?[selectedSizeIndex].sub_category_size_price_after_discount ?? "") + " BHD"
            if product.sizes?.first?.has_discount == "0"{
                detailsCell.discountPriceContainerView.isHidden = true
            }else{
                detailsCell.discountPriceContainerView.isHidden = false
                detailsCell.priceBeforeDiscount.text = (product.sizes?[selectedSizeIndex].sub_category_size_price ?? "") + " BHD"
            }
            detailsCell.desc.text = product.sub_category_desc
            detailsCell.sliders = product.sub_category_image ?? [""]
            detailsCell.isFavouriteImage.image = isFavouriteImage
            detailsCell.pushSizesVcCallBack = {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SizesViewController") as! SizesViewController
                vc.sizes = self.product.sizes ?? []
                vc.selectedSize = self.selectedSizeIndex
                vc.selectedSizeCallBack = { sizeID, sizeName, selectedSize in
                    self.sizeID = sizeID
                    self.sizeName = sizeName
                    self.selectedSizeIndex = selectedSize
                    detailsCell.sizeName.text = self.sizeName
                }
                self.present(vc, animated: true)
            }
            detailsCell.pushColorsVcCallBack = {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColorsViewController") as! ColorsViewController
                vc.colors = self.product.colors ?? []
                vc.selectedColor = self.selectedColorIndex
                vc.selectedColorCallBack = { colorID, colorName, selectedColor in
                    self.colorID = colorID
                    self.colorName = colorName
                    self.selectedColorIndex = selectedColor
                    detailsCell.colorName.text = self.colorName
                }
                self.present(vc, animated: true)
            }
            detailsCell.favouriteButtonCallBack = {
                self.configureFavourites(cell: detailsCell)
            }
            detailsCell.goToGalleryCallBack = { [weak self] sliderShow in
                sliderShow.presentFullScreenController(from: self!)
            }
            detailsCell.pushToPresenytImageCallBack = { selectedImage in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotoesPresenterViewController") as! PhotoesPresenterViewController
                vc.selectedImage = selectedImage
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return detailsCell
        }else{
            ratingCell.reviewData = comments
            ratingCell.pushAddRateVcCallBack = {
                self.configureAddRate()
            }
            ratingCell.pushEditRateVcCallBack = {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditCommentViewController") as! EditCommentViewController
                vc.commentID = self.comments.product?[indexPath.row].comment_id ?? ""
                vc.oldComment = self.comments.product?[indexPath.row].comment ?? ""
                vc.oldRate = self.comments.product?[indexPath.row].rate ?? ""
                self.present(vc, animated: true)
            }
            return ratingCell
        }
    }
}
