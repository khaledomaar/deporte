//
//  LoyaltyPointsViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/22/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class LoyaltyPointsViewController: UIViewController {

    @IBOutlet weak var clientPoints: UILabel!
    @IBOutlet weak var pointsText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Loyalty Points", comment: "")
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getLoyaltyPoints(cartID: "", clientID: ID, addressID: "", vc: self, completionHundler: handleResponse(data:error:))

    }
    func handleResponse (data : loyaltyPoints?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            pointsText.text = data!.points_text
            clientPoints.text = (data?.client_points ?? "") + NSLocalizedString(" Points", comment: "")
        }
    }
}
