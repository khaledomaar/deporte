//
//  SortViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/27/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class SortViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var options = SortFilterOptions(){
        didSet{
            tableView.reloadData()
        }
    }
    var selectedOption = 0
    var selectedOptionID = ""
    var selectedOptionCallBack: ((_ : String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        Services.lookUp(vc: self, completionHundler: handleResponse(data:error:))
    }
    @IBAction func doneButton(_ sender: Any) {
        selectedOptionCallBack?(selectedOptionID)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func handleResponse (data : SortFilterOptions?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.options = data!
            print(options)
        }
    }
    
}
extension SortViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.sorts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell", for: indexPath) as! OptionsTableViewCell
        cell.optionName.text = options.sorts?[indexPath.row].name
        if indexPath.row == selectedOption{
            cell.checkedImage.image = UIImage(named: "check")
            self.selectedOptionID = options.sorts?[selectedOption].id ?? ""
        }else{
            cell.checkedImage.image = UIImage()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOption = indexPath.row
        self.tableView.reloadData()
    }
    
}
