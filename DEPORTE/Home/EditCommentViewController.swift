//
//  EditCommentViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/13/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import Cosmos

class EditCommentViewController: UIViewController {

    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var comment: UITextField!
       
    var commentID = ""
    var oldComment = ""
    var oldRate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        rateView.rating = Double(oldRate) as? Double ?? 0
        comment.text = oldComment
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleEditCommentResponse (data : lightResponse?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            let alert = UIAlertController(title: "", message: NSLocalizedString("You have edit your comment", comment: ""), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { action in
                    
                    let dict = ["lang": "en"]
                    NotificationCenter.default.post(name: Notification.Name("showEditRate"), object: nil, userInfo: dict)

                    self.dismiss(animated: true, completion: nil)
                    
                }))
                self.present(alert, animated: true, completion: nil)
        }
    }

    
    @IBAction func editCommentButton(_ sender: Any) {
        if comment.text != "" {
            Services.editComment(commentID: commentID, comment: comment.text ?? "", rate: "\(rateView.rating)", vc: self, completionHundler: self.handleEditCommentResponse(data:error:))
        }else{
            makeOkAlert(title: NSLocalizedString("You have write your comment", comment: ""), SubTitle: "", Image: UIImage())
        }
    }
}
