//
//  AddCommentViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/13/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import Cosmos

class AddCommentViewController: UIViewController {

    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var comment: UITextField!
    
    var subCatID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitCommentButton(_ sender: Any) {
        if comment.text != "" {
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.addComment(clientId: ID, subCatId: subCatID, comment: comment.text ?? "", rate: "\(rateView.rating)", vc: self, completionHundler: handleResponse(data:error:))
        }else{
            makeOkAlert(title: NSLocalizedString("You have write your comment", comment: ""), SubTitle: "", Image: UIImage())
        }
        
    }
    func handleResponse (data : AddComment?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            let alert = UIAlertController(title: "", message: NSLocalizedString("You have submit your comment", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Done", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                
                let dict = ["lang": "en"]
                NotificationCenter.default.post(name: Notification.Name("showRate"), object: nil, userInfo: dict)
                
                self.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
