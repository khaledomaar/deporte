//
//  SubCategoriesViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class SubCategoriesViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var parentImage: UIImageView!
    
    var parentID = ""
    var parentCatImage = ""
    var subParent = SubParent(){
        didSet{
            tableView.reloadData()
        }
    }
    var catTile = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = catTile

        tableView.delegate = self
        tableView.dataSource = self
        parentImage.sd_setImage(with: URL(string: parentCatImage))
        Services.getSubParent(parentID: parentID, vc: self, completionHundler: handleResponse(data:error:))
    }
    func handleResponse (data : SubParent?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.subParent = data!
            print(subParent)
        }
    }
}
extension SubCategoriesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subParent.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTableViewCell", for: indexPath) as! SubCategoryTableViewCell
        cell.name.text = subParent.data?[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
        vc.subParentID = subParent.data?[indexPath.row].id ?? ""
        vc.parentCatID = subParent.data?[indexPath.row].parent_category_id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
