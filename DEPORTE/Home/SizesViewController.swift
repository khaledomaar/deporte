//
//  SizesViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/17/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class SizesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var sizes = [SizesData]()
    var selectedSize = 0
    var selectedSizeId = ""
    var selectedSizeName = ""
    var selectedSizeCallBack: ((_ sizeID: String, _ sizeName: String, _ selectedSize: Int)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
    }
    @IBAction func pickSizeButton(_ sender: Any) {
        self.selectedSizeCallBack?(selectedSizeId, selectedSizeName, selectedSize)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension SizesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sizes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell", for: indexPath) as! OptionsTableViewCell
        cell.optionName.text = sizes[indexPath.row].sub_category_size_name
        cell.sizePrice.text = (sizes[indexPath.row].sub_category_size_price ?? "") + " BHD"
        if indexPath.row == selectedSize{
            cell.checkedImage.image = UIImage(named: "check")
            selectedSizeId = sizes[indexPath.row].sub_category_size_price_id ?? ""
            selectedSizeName = sizes[indexPath.row].sub_category_size_name ?? ""
        }else{
            cell.checkedImage.image = UIImage()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSize = indexPath.row
        self.tableView.reloadData()
    }
    
}
