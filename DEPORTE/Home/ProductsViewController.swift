//
//  ProductsViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/13/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import SDWebImage

class ProductsViewController: BaseViewController {
    
    @IBOutlet weak var listAndLargeImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var layoutControlImage: UIImageView!
    
    var isFromFilter = false
    var products: SubCategories?{
        didSet{
            collectionView.reloadData()
        }
    }
    
    var list = false{
        didSet{
            collectionView.reloadData()
        }
    }
    var parentCatID = ""
    var subParentID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Products", comment: "")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewLargeLayOut()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if isFromFilter == false{
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.getSubCat(parentCategoryId: parentCatID, subParentID: subParentID, clientId: ID, vc: self, completionHundler: handleResponse(data:error:))
        }
    }
    func handleResponse (data : SubCategories?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.products = data!
        }
    }
    
    @IBAction func largeAndListButton(_ sender: Any) {
        if list == false{
            list = true
            collectionViewListLayOut()
        }else{
            list = false
            collectionViewLargeLayOut()
        }
    }
    @IBAction func sortButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SortViewController") as! SortViewController
        vc.selectedOptionCallBack = { optionID in
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.sort(clientID: ID, key: optionID, vc: self, completionHundler: self.handleResponse(data:error:))
        }
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func filterButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        vc.selectedOptionCallBack = { key, keyValue in
            self.isFromFilter = true
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.filter(clientID: ID, key: key, keyValue: keyValue, vc: self, completionHundler: self.handleResponse(data:error:))
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionViewLargeLayOut() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 15, bottom: 10, right: 15)
        layout.itemSize = CGSize(width: self.view.frame.width / 2 - 30 , height: 230.0)
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 3
        collectionView.collectionViewLayout = layout
        layoutControlImage.image = UIImage(named: "align-left")
    }
    func collectionViewListLayOut() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 15, bottom: 10, right: 15)
        layout.itemSize = CGSize(width: self.view.frame.width - 30 , height: 120)
        collectionView.collectionViewLayout = layout
        layoutControlImage.image = UIImage(named: "th-large")
    }
}
extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.product?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let listCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsInListCollectionViewCell", for: indexPath) as! ProductsInListCollectionViewCell
        let largeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsInLargeCollectionViewCell", for: indexPath) as! ProductsInLargeCollectionViewCell
        if list == true{
            listCell.name.text = products?.product?[indexPath.item].sub_category_name
            listCell.price.text = (products?.product?[indexPath.item].sizes?.first?.sub_category_size_price_after_discount ?? "") + " BHD"
            if products?.product?[indexPath.item].sizes?.first?.has_discount == "0"{
                listCell.discountPriceContainerView.isHidden = true
            }else{
                listCell.discountPriceContainerView.isHidden = false
                listCell.priceBeforeDisc.text = (products?.product?[indexPath.item].sizes?.first?.sub_category_size_price ?? "") + " BHD"
            }
            listCell.productImage.sd_setImage(with: URL(string: products?.product?[indexPath.item].sub_category_image?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: UIImage(named: "Untitled-1"))

            
            return listCell
        }else{
            largeCell.name.text = products?.product?[indexPath.item].sub_category_name
            largeCell.price.text = (products?.product?[indexPath.item].sizes?.first?.sub_category_size_price_after_discount ?? "") + " BHD"
            if products?.product?[indexPath.item].sizes?.first?.has_discount == "0"{
                largeCell.discountPriceContainerView.isHidden = true
            }else{
                largeCell.discountPriceContainerView.isHidden = false
                largeCell.priceBeforeDisc.text = (products?.product?[indexPath.item].sizes?.first?.sub_category_size_price ?? "") + " BHD"
            }
            largeCell.productImage.sd_setImage(with: URL(string: products?.product?[indexPath.item].sub_category_image?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: UIImage(named: "Untitled-1"))
            
            return largeCell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        vc.product = self.products?.product?[indexPath.item] ?? SubCatData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
