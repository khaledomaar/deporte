//
//  CategoriesViewController.swift
//  DEPORTE
//
//  Created by khaled omar on 9/14/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class CategoriesViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories = ParentCat(){
        didSet{
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("Categories", comment: "")

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewLayOut()
        Services.getParentCats(vc: self, completionHundler: handleResponse(data:error:))
    }
    func collectionViewLayOut() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
        layout.itemSize = CGSize(width: self.view.frame.width / 2 - 10 , height: 265.0)
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 3
        collectionView.collectionViewLayout = layout
    }
    func handleResponse (data : ParentCat?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.categories = data!
            print(categories)
        }
    }
}
extension CategoriesViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.product?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.name.text = categories.product?[indexPath.item].parent_category_name
        cell.categoryImage.sd_setImage(with: URL(string: categories.product?[indexPath.item].parent_category_image ?? ""))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoriesViewController") as! SubCategoriesViewController
        vc.parentID = categories.product?[indexPath.item].parent_category_id ?? ""
        vc.parentCatImage = categories.product?[indexPath.item].parent_category_image ?? ""
        vc.catTile = categories.product?[indexPath.item].parent_category_name ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
