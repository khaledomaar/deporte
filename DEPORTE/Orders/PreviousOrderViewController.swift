//
//  PreviousOrderViewController.swift
//  Anhar
//
//  Created by khaled omar on 4/7/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import UIKit

class PreviousOrderViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyOrderView: UIView!
    
    var lastOrder = Order(){
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
            super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        emptyOrderView.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getPreviosOrder(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
    
    func handleResponse (data : Order?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            lastOrder = data!
            print(lastOrder)
            if lastOrder.product?.count == 0 {
                self.tableView.isHidden = true
                emptyOrderView.isHidden = false
            }else{
                self.tableView.isHidden = false
                emptyOrderView.isHidden = true
            }
        }
    }
}
extension PreviousOrderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return lastOrder.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lastOrder.product?[section].opened == true {
            return (lastOrder.product?[section].items?.count ?? 0) + 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LastOrdersTableViewCell", for: indexPath) as! LastOrdersTableViewCell
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: "LastDetailsTableViewCell", for: indexPath) as! LastDetailsTableViewCell
        if indexPath.row == 0 {
            cell.dateOrder.text = lastOrder.product?[indexPath.section].order_date ?? ""
            cell.totalPrice.text = NSLocalizedString("Total Amount: ", comment: "") + "\(lastOrder.product?[indexPath.section].net_price ?? "0") " + NSLocalizedString(" BHD", comment: "")
            cell.orderNumber.text = NSLocalizedString("Order Number: ", comment: "") + "\(lastOrder.product?[indexPath.section].order_id ?? "0") "
            return cell
        }else{
            detailsCell.dishName.text = lastOrder.product?[indexPath.section].items?[indexPath.row - 1].sub_category_name
            detailsCell.unit.text = (lastOrder.product?[indexPath.section].items?[indexPath.row - 1].size_price ?? "0") + NSLocalizedString(" BHD", comment: "")
            detailsCell.quantity.text = lastOrder.product?[indexPath.section].items?[indexPath.row - 1].quantity
            detailsCell.color.text = lastOrder.product?[indexPath.section].items?[indexPath.row - 1].color_name
            detailsCell.size.text = lastOrder.product?[indexPath.section].items?[indexPath.row - 1].size_name
            detailsCell.price.text = (lastOrder.product?[indexPath.section].items?[indexPath.row - 1].price ?? "0") + NSLocalizedString(" BHD", comment: "")
            detailsCell.dishImage.sd_setImage(with: URL(string: lastOrder.product?[indexPath.section].items?[indexPath.row - 1].sub_category_images?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" ))
            
            detailsCell.vatPersentage.text = NSLocalizedString("Vat", comment: "") + " (\(lastOrder.product?[indexPath.section].vat ?? "0")%) :"
            detailsCell.vatValue.text = "\(lastOrder.product?[indexPath.section].vat_added ?? "")" + NSLocalizedString(" BHD", comment: "")
            detailsCell.totalAfter.text = "\(lastOrder.product?[indexPath.section].net_price ?? "")" + NSLocalizedString(" BHD", comment: "")
            detailsCell.discountValue.text = "\(lastOrder.product?[indexPath.section].discount_value ?? "")" + NSLocalizedString(" BHD", comment: "")
            detailsCell.dicountPersentage.text = NSLocalizedString("Discount" , comment: "") + " (\(lastOrder.product?[indexPath.section].discount_percentage ?? "")%) :"
            detailsCell.chargeCost.text = "\(lastOrder.product?[indexPath.section].charge_cost ?? "")" + NSLocalizedString(" BHD", comment: "")
            detailsCell.totalAmount.text = "\(lastOrder.product?[indexPath.section].total_price ?? "")" + NSLocalizedString(" BHD", comment: "")
            
            return detailsCell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if lastOrder.product?[indexPath.section].opened == true{
            lastOrder.product?[indexPath.section].opened = false
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadData()
        }else{
            lastOrder.product?[indexPath.section].opened = true
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadData()
        }
    }
        
}
