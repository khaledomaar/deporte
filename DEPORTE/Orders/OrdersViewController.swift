//
//  OrdersViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/8/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import Parchment

class OrdersViewController: BaseViewController {
    
    @IBOutlet weak var parchementView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.title = NSLocalizedString("My Orders", comment: "")
        
        let secondViewController = self.storyboard?.instantiateViewController(identifier: "CurrentOrdersViewController") as! CurrentOrdersViewController
        let firstViewController = self.storyboard?.instantiateViewController(identifier: "PreviousOrderViewController") as! PreviousOrderViewController
        firstViewController.title = NSLocalizedString("Previous Orders", comment: "")
        secondViewController.title = NSLocalizedString("Current Orders", comment: "")

        var pagingViewController = PagingViewController(viewControllers: [firstViewController,secondViewController
        ])

        if LanguageManager.shared.currentLanguage == .en {
            pagingViewController = PagingViewController(viewControllers: [secondViewController,firstViewController
            ])
        }else{
            pagingViewController.select(index: 1)
        }
          
        pagingViewController.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: view.frame.width/2 , height: 60 )
        pagingViewController.textColor = #colorLiteral(red: 0.4399505556, green: 0.4400306344, blue: 0.439945519, alpha: 1)
        pagingViewController.backgroundColor = #colorLiteral(red: 0.9214832783, green: 0.9216412902, blue: 0.9214733243, alpha: 1)
        pagingViewController.selectedBackgroundColor = #colorLiteral(red: 0.9214832783, green: 0.9216412902, blue: 0.9214733243, alpha: 1)
        pagingViewController.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        pagingViewController.selectedFont = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        pagingViewController.selectedTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        pagingViewController.indicatorColor = #colorLiteral(red: 0.4399505556, green: 0.4400306344, blue: 0.439945519, alpha: 1)
        pagingViewController.indicatorOptions = .visible(height: 5, zIndex: .zero, spacing: .zero, insets: .zero)
        pagingViewController.selectedScrollPosition = .preferCentered
        addChild(pagingViewController)
        parchementView.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pagingViewController.view.leadingAnchor.constraint(equalTo: parchementView.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: parchementView.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: parchementView.bottomAnchor),
            pagingViewController.view.topAnchor.constraint(equalTo: parchementView.topAnchor )
        ])
    }

}
