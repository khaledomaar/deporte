//
//  LastOrdersTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/10/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class LastOrdersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateOrder: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
