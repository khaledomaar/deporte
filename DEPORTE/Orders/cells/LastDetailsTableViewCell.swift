//
//  LastDetailsTableViewCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/15/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class LastDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var size: UILabel!
    
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var dicountPersentage: UILabel!
    @IBOutlet weak var discountValue: UILabel!
    @IBOutlet weak var vatPersentage: UILabel!
    @IBOutlet weak var vatValue: UILabel!
    @IBOutlet weak var chargeCost: UILabel!
    @IBOutlet weak var totalAfter: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
