//
//  TrackCurrentOrderCell.swift
//  DarElzain
//
//  Created by khaled omar on 5/15/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class TrackCurrentOrderCell: UITableViewCell {

    @IBOutlet weak var preparingStatusImage: UIImageView!
    @IBOutlet weak var onWayStatusImage: UIImageView!
    @IBOutlet weak var deliveredStatusImage: UIImageView!
    @IBOutlet weak var preparingStatusView: UIView!
    @IBOutlet weak var deliveredStatusView: UIView!
    
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var dicountPersentage: UILabel!
    @IBOutlet weak var discountValue: UILabel!
    @IBOutlet weak var vatPersentage: UILabel!
    @IBOutlet weak var vatValue: UILabel!
    @IBOutlet weak var chargeCost: UILabel!
    @IBOutlet weak var totalAfter: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
