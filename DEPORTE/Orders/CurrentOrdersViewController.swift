//
//  CurrentOrdersViewController.swift
//  Anhar
//
//  Created by khaled omar on 4/7/20.
//  Copyright © 2020 khaled omar. All rights reserved.
//

import UIKit

class CurrentOrdersViewController: BaseViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyOrderView: UIView!
    
    var currentOrder = Order(){
        didSet{
            tableView.reloadData()
        }
    }
        
    override func viewDidLoad() {
            super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        emptyOrderView.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getCurrentOrder(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
    func handleResponse (data : Order?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            currentOrder = data!
            print(currentOrder)
            if currentOrder.product?.count == 0 {
                self.tableView.isHidden = true
                emptyOrderView.isHidden = false
            }else{
                self.tableView.isHidden = false
                emptyOrderView.isHidden = true
            }
        }
    }
}
extension CurrentOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentOrder.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentOrder.product?[section].opened == true {
            return (currentOrder.product?[section].items?.count ?? 0) + 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentOrdersTableViewCell", for: indexPath) as! CurrentOrdersTableViewCell
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: "CurrentDetailsTableViewCell", for: indexPath) as! CurrentDetailsTableViewCell
        let trackCell = tableView.dequeueReusableCell(withIdentifier: "TrackCurrentOrderCell", for: indexPath) as! TrackCurrentOrderCell
        if indexPath.row == 0 {
            cell.dateOrder.text = currentOrder.product?[indexPath.section].order_date ?? ""
            cell.totalPrice.text = NSLocalizedString("Total Amount: ", comment: "") + "\(currentOrder.product?[indexPath.section].net_price ?? "0") " + NSLocalizedString(" BHD", comment: "")
            cell.orderNumber.text = NSLocalizedString("Order Number: ", comment: "") + "\(currentOrder.product?[indexPath.section].order_id ?? "0") "
            return cell
        }else if indexPath.row == (currentOrder.product?[indexPath.section].items?.count ?? 0) + 1 {
            if currentOrder.product?[indexPath.section].order_status == "1" {
                if currentOrder.product?[indexPath.section].order_follow == "1" {
                    trackCell.preparingStatusImage.image = UIImage(named: "followGreen")
                    trackCell.preparingStatusView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                }else if currentOrder.product?[indexPath.section].order_follow == "2" {
                    trackCell.preparingStatusImage.image = UIImage(named: "followGreen")
                    trackCell.preparingStatusView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                    trackCell.onWayStatusImage.image = UIImage(named: "followGreen")
                }else if currentOrder.product?[indexPath.section].order_follow == "3" {
                    trackCell.preparingStatusImage.image = UIImage(named: "followGreen")
                    trackCell.preparingStatusView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                    trackCell.onWayStatusImage.image = UIImage(named: "followGreen")
                    trackCell.deliveredStatusImage.image = UIImage(named: "followGreen")
                    trackCell.deliveredStatusView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                }
            }else if currentOrder.product?[indexPath.section].order_status == "1" {
                print("rejected order")
            }
            trackCell.vatPersentage.text = NSLocalizedString("Vat", comment: "") + " (\(currentOrder.product?[indexPath.section].vat ?? "0")%) :"
            trackCell.vatValue.text = "\(currentOrder.product?[indexPath.section].vat_added ?? "")" + NSLocalizedString(" BHD", comment: "")
            trackCell.totalAfter.text = "\(currentOrder.product?[indexPath.section].net_price ?? "")" + NSLocalizedString(" BHD", comment: "")
            trackCell.discountValue.text = "\(currentOrder.product?[indexPath.section].discount_value ?? "")" + NSLocalizedString(" BHD", comment: "")
            trackCell.dicountPersentage.text = NSLocalizedString("Discount" , comment: "") + " (\(currentOrder.product?[indexPath.section].discount_percentage ?? "")%) :"
            trackCell.chargeCost.text = "\(currentOrder.product?[indexPath.section].charge_cost ?? "")" + NSLocalizedString(" BHD", comment: "")
            trackCell.totalAmount.text = "\(currentOrder.product?[indexPath.section].total_price ?? "")" + NSLocalizedString(" BHD", comment: "")
            return trackCell
        
        }else {
            detailsCell.dishName.text = currentOrder.product?[indexPath.section].items?[indexPath.row - 1].sub_category_name
            detailsCell.unit.text = (currentOrder.product?[indexPath.section].items?[indexPath.row - 1].size_price ?? "0") + NSLocalizedString(" BHD", comment: "")
            detailsCell.quantity.text = currentOrder.product?[indexPath.section].items?[indexPath.row - 1].quantity
            detailsCell.color.text = currentOrder.product?[indexPath.section].items?[indexPath.row - 1].color_name
            detailsCell.size.text = currentOrder.product?[indexPath.section].items?[indexPath.row - 1].size_name
            detailsCell.price.text = (currentOrder.product?[indexPath.section].items?[indexPath.row - 1].price ?? "0") + NSLocalizedString(" BHD", comment: "")
            detailsCell.dishImage.sd_setImage(with: URL(string: currentOrder.product?[indexPath.section].items?[indexPath.row - 1].sub_category_images?.first?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" ))
            
        }

            return detailsCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentOrder.product?[indexPath.section].opened == true{
            currentOrder.product?[indexPath.section].opened = false
            let sections = IndexSet.init(integer: indexPath.section)
//            tableView.reloadSections(sections, with: .none)
            tableView.reloadData()
        }else{
            currentOrder.product?[indexPath.section].opened = true
            let sections = IndexSet.init(integer: indexPath.section)
//            tableView.reloadSections(sections, with: .none)
            tableView.reloadData()
        }
    }
}
