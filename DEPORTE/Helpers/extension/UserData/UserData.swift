//
//  UserData.swift
//  iBety
//
//  Created by Mohamed Nawar on 6/11/19.
//  Copyright © 2019 Eslam. All rights reserved.
//


import Foundation
struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    
    var data : UserDetails?
    var token : String?

    var firstUse:String?
    
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")
        data = nil
        token = ""
    }
    
    mutating func removeIdentifierInside() {
        UserDefaults.standard.removeObject(forKey: "identifier")
    }
    mutating func removeToken() {
        UserDefaults.standard.removeObject(forKey: "token")
    }

    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "token") {
            self.token = String(data)
        }

        if let  data = UserDefaults.standard.string(forKey: "firstUse") {
            self.firstUse = String(data)
        }
    }
}
struct CurrentUser:Decodable {
    var product: [UserDetails]?
    var success : Int?
}
struct UserDetails : Decodable {
    var client_id : String?
    var client_name : String?
    var client_password : String?
    var client_email : String?
    var client_phone : String?
    var client_image : String?
    var is_parent : String?
    var parent_id : String?
}




