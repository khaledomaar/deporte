//
//  ChatComplaintsViewController.swift
//  CocoDip
//
//  Created by khaled omar on 6/4/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class ChatComplaintsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addMessageText: UITextField!
    
    
    var complaintsID = ""
    
    var messages = GetMessages(){
        didSet{
            tableView.reloadData()
        }
    }
    var addMessage = AddMessage(){
        didSet{
            Services.getmagsByComplaintID(complaintID: complaintsID, vc: self, completionHundler: handleResponse(data:error:))
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Inbox", comment: "")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Services.getmagsByComplaintID(complaintID: complaintsID, vc: self, completionHundler: handleResponse(data:error:))
    }
    @IBAction func addMessageButton(_ sender: Any) {
        if addMessageText.text == "" {
            print("not true")
        }else{
            let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
            Services.addMessage(clientId: ID, content: addMessageText.text ?? "", complaint_id: complaintsID, vc: self, completionHundler: handleAddMessageResponse(data:error:))
        }
    }
    func handleAddMessageResponse (data : AddMessage?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.addMessage = data!
            self.addMessageText.text = ""
            tableView.setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: false)

        }
    }
    
    func handleResponse (data : GetMessages?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.messages = data!
            tableView.setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: false)

        }
    }
}
extension ChatComplaintsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userCell = tableView.dequeueReusableCell(withIdentifier: "UserChatTableViewCell", for: indexPath) as! UserChatTableViewCell
        let technicalCell = tableView.dequeueReusableCell(withIdentifier: "TechnicalSupportChatCell", for: indexPath) as! TechnicalSupportChatCell
        if messages.product?[indexPath.row].type == "1" {
            userCell.userMessage.text = messages.product?[indexPath.row].content
            return userCell
        }else{
            technicalCell.technicalMessage.text = messages.product?[indexPath.row].content
            return technicalCell
        }
        
    }
    
    
}
