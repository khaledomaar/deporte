//
//  TechnicalSupportChatCell.swift
//  CocoDip
//
//  Created by khaled omar on 6/4/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class TechnicalSupportChatCell: UITableViewCell {

    @IBOutlet weak var technicalMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
