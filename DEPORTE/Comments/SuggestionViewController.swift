//
//  SuggestionViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit

class SuggestionViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyOrderView: UIView!
    
    var complaints = GetComplaints(){
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        emptyOrderView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let ID = UserDefaults.standard.string(forKey: "clientID") ?? ""
        Services.getComplaints(clientID: ID, vc: self, completionHundler: handleResponse(data:error:))
    }
    func handleResponse (data : GetComplaints?, error : String?){
        if error != nil {
            // lw fe error
            self.makeDoneAlert(title: "", SubTitle: error!, Image: UIImage())
        }else if error == nil && data == nil {
            // lw mfesh wla data wla error
            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Try Again Later!!", comment: ""), Image: UIImage())
        }else {
            // lw fe data w mfes herror
            self.complaints = data!
            if complaints.product?.count == 0 {
                self.tableView.isHidden = true
                emptyOrderView.isHidden = false
            }else{
                self.tableView.isHidden = false
                emptyOrderView.isHidden = true
            }
        }
    }
}

extension SuggestionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complaints.product?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferTableViewCell", for: indexPath) as! OfferTableViewCell
        cell.dateLbl.text = complaints.product?[indexPath.row].date ?? ""
        cell.titleLbl.text = complaints.product?[indexPath.row].content ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "ChatComplaintsViewController") as! ChatComplaintsViewController
        vc.complaintsID = complaints.product?[indexPath.row].complaint_id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
                cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
            },completion: { finished in
                UIView.animate(withDuration: 0.1, animations: {
                    cell.layer.transform = CATransform3DMakeScale(1,1,1)
                })
        })
    }
}
