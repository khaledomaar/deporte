//
//  CommentsViewController.swift
//  DarElzain
//
//  Created by khaled omar on 5/9/20.
//  Copyright © 2020 emcan. All rights reserved.
//

import UIKit
import Parchment

class CommentsViewController: BaseViewController {
    
    @IBOutlet weak var parchementView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = NSLocalizedString("Comments", comment: "")

        let firstViewController = self.storyboard?.instantiateViewController(identifier: "SuggestionViewController") as! SuggestionViewController
        let secondViewController = self.storyboard?.instantiateViewController(identifier: "OffersViewController") as! OffersViewController
        let thirdViewController = self.storyboard?.instantiateViewController(identifier: "OccasionsViewController") as! OccasionsViewController
        firstViewController.title = NSLocalizedString("Compliants", comment: "")
        secondViewController.title = NSLocalizedString("Offers", comment: "")
        thirdViewController.title = NSLocalizedString("Events", comment: "")


        var pagingViewController = PagingViewController(viewControllers: [firstViewController,secondViewController, thirdViewController
        ])
        if LanguageManager.shared.currentLanguage == .en{
            pagingViewController.select(index: 0)
        }else{
            pagingViewController = PagingViewController(viewControllers: [thirdViewController,secondViewController, firstViewController
            ])
            pagingViewController.select(index: 2)
        }
          
        pagingViewController.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: view.frame.width/3.5 , height: 60 )
        pagingViewController.textColor = #colorLiteral(red: 0.4399505556, green: 0.4400306344, blue: 0.439945519, alpha: 1)
        pagingViewController.backgroundColor = #colorLiteral(red: 0.9214832783, green: 0.9216412902, blue: 0.9214733243, alpha: 1)
        pagingViewController.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        pagingViewController.selectedFont = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        pagingViewController.selectedBackgroundColor = #colorLiteral(red: 0.9214832783, green: 0.9216412902, blue: 0.9214733243, alpha: 1)
        pagingViewController.selectedTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        pagingViewController.indicatorColor = #colorLiteral(red: 0.4399505556, green: 0.4400306344, blue: 0.439945519, alpha: 1)
        pagingViewController.indicatorOptions = .visible(height: 8, zIndex: .zero, spacing: .zero, insets: .zero)
        pagingViewController.selectedScrollPosition = .preferCentered
        addChild(pagingViewController)
        parchementView.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pagingViewController.view.leadingAnchor.constraint(equalTo: parchementView.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: parchementView.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: parchementView.bottomAnchor),
            pagingViewController.view.topAnchor.constraint(equalTo: parchementView.topAnchor )
        ])
    }

}
